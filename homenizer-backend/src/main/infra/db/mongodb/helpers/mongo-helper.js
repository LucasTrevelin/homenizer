import * as dotenv from 'dotenv'
import db from "../../../../models/index.js";
import { initial } from "./initial-helper.js";

dotenv.config();

export const mongoHelper = async() => {

  db.mongoose.set('strictQuery', true);
// utilizar o método .once é melhor do que try/catch porque deixa o código mais claro, tem melhor performance, permite 'isolar' os erros de abertura de conexão e é boa prática recomendada pela mongoose ao invés de try/catch. O método .once deve ser utilizado sempre com o método .on.
  db.mongoose.connection.once('open', () => {

    console.log("Server connected to MongoDB");
    initial();

  }).on('error', (err) => {
    console.error("Connection error", err);
    process.exit();
  });
  db.mongoose.connect(`mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}/?retryWrites=true&w=majority`, {
          useNewUrlParser: true,
          useUnifiedTopology: true
  }); 

}
