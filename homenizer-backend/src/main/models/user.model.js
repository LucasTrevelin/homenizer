import mongoose from "mongoose";

const User = mongoose.model("User", new mongoose.Schema({
    username: String,
    email: String,
    password: String,
    house: 
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: "House"
      },
    roles: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Role"
        }
    ]
}))

export {User}