import mongoose from "mongoose";

import {User} from './user.model.js'
import {Role} from './role.model.js'
import {Note} from './notes.model.js'
import {House} from './house.model.js'
import { PantryItem } from "./pantryItem.model.js";
import { ShopList } from "./shopList.model.js";
import { ShopListItem } from "./shopListItem.model.js";


mongoose.Promise = global.Promise

const db = {}

db.mongoose = mongoose

db.user = User

db.role = Role

db.note = Note

db.house = House

db.pantryitem = PantryItem

db.shoplist = ShopList

db.ROLES = ["user", "admin"]

export default db