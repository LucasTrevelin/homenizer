import mongoose from "mongoose";

const Note = mongoose.model("Note", new mongoose.Schema({
    message: {type: String},
    favorite: {type: Boolean, default: false},
    author: String,
    editionAuthor: String,
    house: 
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: "House"
      },
}))

export {Note}