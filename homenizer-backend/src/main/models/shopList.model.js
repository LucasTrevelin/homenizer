import mongoose from "mongoose";

const ShopList = mongoose.model("ShopList", new mongoose.Schema({
    name: String,
    lastUpdate: Date,
    shopListItems:[ 
      {
        name: String,
        quantity: Number,
        checked: Boolean,
      }
    ],
    house: 
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: "House"
      },
}))

export {ShopList}