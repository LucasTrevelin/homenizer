import mongoose from "mongoose";

const PantryItem = mongoose.model("PantryItem", new mongoose.Schema({
    name: String,
    quantity: Number,
    house: 
      {
          type: mongoose.Schema.Types.ObjectId,
          ref: "House"
      },
}))

export {PantryItem}