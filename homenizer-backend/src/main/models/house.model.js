import mongoose from "mongoose";

const House = mongoose.model("House", new mongoose.Schema({
    name: String,
    users: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
  }],
    notes: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Note"
    }],
    pantryItems: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "PantryItem"
    }],
    shopLists: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "ShopList"
    }]
}))

export {House}