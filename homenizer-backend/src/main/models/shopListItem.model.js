import mongoose from "mongoose";

const ShopListItem = mongoose.model("ShopListItem", new mongoose.Schema({
    name: String,
    quantity: Number,
    checked: Boolean,
}))

export {ShopListItem}