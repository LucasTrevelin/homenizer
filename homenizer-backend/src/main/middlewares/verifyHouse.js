import db from '../models/index.js'

const House = db.house

const checkHouseDuplicity = async(req,res,next) => {

  
  const searchedHouse = await House.findOne({name: req.body.name})

  if (searchedHouse){
    res.status(400).send({message: "Casa não criada: já existe uma casa com este nome."})
    return;
  }
  next();
}

  const verifyHouse = {
    checkHouseDuplicity,
  };

  export {verifyHouse}