import db from '../models/index.js'

const ROLES = db.ROLES
const User = db.user

const checkDuplicateUsernameOrEmail = async(req,res,next) => {

    // Username
   const username = await User.findOne({
        username: req.body.username
    })

    if (username){
      res.status(400).send({message: "Falha no cadastro! Este nome já está em uso!"})
       return;
   }


    // Email
    const email = await User.findOne({
        email: req.body.email
    })
      if (email){
        res.status(400).send({message: "Falha no cadastro! Este e-mail já está em uso!"})
         return
     }
      next();
}

const checkRolesExisted = (req, res, next) => {
    if (req.body.roles) {
      for (let i = 0; i < req.body.roles.length; i++) {
        if (!ROLES.includes(req.body.roles[i])) {
          res.status(400).send({
            message: `Falha! Função ${req.body.roles[i]} não existe!`
          });
          return;
        }

      }
    }
    next();
  };

  const verifyUser = {
    checkDuplicateUsernameOrEmail,
    checkRolesExisted
  };

  export {verifyUser}