import jwt from 'jsonwebtoken'

import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config()
const jwtSecretKey = process.env.JWT_SECRET_KEY || "JWT-SUPER-SECRET-KEY"

const User = db.user;
const Role = db.role;

const verifyToken = (req, res, next) => {
    let token = req.headers["authorization"];
    if (!token) {
      return res.status(403).send({ message: "Acesso proibido!" });
    }
    jwt.verify(token, jwtSecretKey, (err, decoded) => {
      if (err) {
        return res.status(401).send({ message: "Acesso negado!" });
      }
      req.userId = decoded.id;
      next();
    });
  };

  const isAdmin = (req, res, next) => {
    User.findById(req.userId).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      Role.find(
        {
          _id: { $in: user.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          for (let i = 0; i < roles.length; i++) {
            if (roles[i].name === "admin") {
              next();
              return;
            }
          }
          res.status(403).send({ message: "Require Admin Role!" });
          return;
        }
      );
    });
  };

  const authJwt = {
    verifyToken,
    isAdmin,
  };

  export {authJwt}

