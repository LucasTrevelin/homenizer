import mongoose from 'mongoose'
import db from '../models/index.js'

const PantryItem = db.pantryitem

const checkPantryItemDuplicity = async(req,res,next) => {
  const searchedPantryItem = await PantryItem.findOne( { name: req.body.name}).collation({ locale: 'en', strength: 2 })
  if (searchedPantryItem && req.body.houseId === mongoose.Types.ObjectId(searchedPantryItem.house).toString()){
    res.status(400).send({message: "Item não criado: já existe um item com este nome na despensa."})
    return;
  }
  next();
}

const checkPositiveQuantityItemUpdate = async(req, res, next) => {
  const searchedPantryItem = await PantryItem.findById({_id: req.body._id})

  if (!searchedPantryItem){
    return res.status(404).send({message: "Item não encontrado no banco de dados."})
  }
  if (req.body.changes.quantity < 0){
    return res.status(404).send({message: "Item não pode ter quantidades negativas."})
  }

  next()
}

  const verifyPantryItem = {
    checkPantryItemDuplicity,
    checkPositiveQuantityItemUpdate
  };

  export {verifyPantryItem}