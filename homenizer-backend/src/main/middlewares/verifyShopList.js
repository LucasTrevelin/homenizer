import mongoose from 'mongoose'
import db from '../models/index.js'

const ShopList = db.shoplist

const checkShopListDuplicity = async(req,res,next) => {
  const searchedShopList = await ShopList.findOne( { name: req.body.name}).collation({ locale: 'en', strength: 2 })
  if (searchedShopList && mongoose.Types.ObjectId( searchedShopList.house).toString() === req.body.houseId){
    res.status(400).send({message: "Item não criado: já existe uma lista com este nome."})
    return;
  }
  next();
}

  const verifyShopList = {
    checkShopListDuplicity,
  };

  export {verifyShopList}