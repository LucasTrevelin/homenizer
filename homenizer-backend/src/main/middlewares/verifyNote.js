import mongoose from 'mongoose'
import db from '../models/index.js'

const Note = db.note

const checkDuplicateNote = async(req,res,next) => {
  
    // Note
   const note = await Note.findOne({
        message: req.body.message
    })
    if (note && req.body.houseId === mongoose.Types.ObjectId(note.house).toString()){
      res.status(400).send({message: "Falha na criação. Este recado já existe!"})
       return;
   }
      next();
}

const verifyNote = {
  checkDuplicateNote,
};

export {verifyNote}