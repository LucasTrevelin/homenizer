import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config();

const House = db.house

const User = db.user

const Note = db.note

const Role = db.role

const PantryItem = db.pantryitem

const ShopList = db.shoplist


export const createHouse = async(req,res) => {
  const newUsers = [...req.body.users, req.body.adminUser]
  const house = new House({
      name: req.body.name,
      users: newUsers,
      adminUser: req.body.adminUser
  })
  const {name, users, adminUser} = req.body

  if (!name){
    return res.status(400).send({message: 'Casa não criada, um nome é necessário.'})
  }

  if (!adminUser){
    return res.status(400).send({message: 'Casa não criada, usuário administrador da casa é necessário.'})
  }

  const findRoleByName = async (roleName) => {
    const completeRole =  await Role.find({name: { $in: roleName}})
    return completeRole
  }
  
  //change roleName to roleId
  const roleName =  adminUser.roles
  const adminRole = await findRoleByName(roleName)


  //Update adminUser as admin and house associated
  await User.findByIdAndUpdate({_id: adminUser._id}, {house: house._id, roles: adminRole}).then(
  ).catch((err) => {
    res.status(500).send({message: err});
    return;
  })

  //Create house
     house.save((err, house) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
    });

  //update house associated for each user
  if (users.length){
    await users.forEach(userId => {
    User.findByIdAndUpdate({_id: userId}, {house: house} , (err, user) => {
      if (err){
        res.status(500).send({message: err});
        return
      }
    })
  })}
  return res.status(201).send({message: 'Casa criada com sucesso!', data: house})
}

export const getAll = async(req,res) => {
  House.find({}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const getHouseById = async(req,res) => {
  const {_id} = req.query
  if (!_id){
    res.status(400).send({message: 'Casa não encontrada, é preciso enviar o Id.'})
    return
  }
    House.findById({_id}).populate('users').populate([
        {
        path: 'users',
        model: 'User'
        }, 
        {
        path: 'users',
        populate: {
          path: 'roles'
        }
      }]
      ).exec((err, house) => {
      if (err) {res.status(500).send({message: err})
        return;
      }
      res.status(200).json(house)
    })
    return
}

export const deleteById = async(req,res) => {
  await House.deleteOne({_id: req.body._id}).then(async() => {
    if (!req.body._id){
      res.status(404).send({message: "Casa não encontrada."});
      return;
   }

   await User.findByIdAndUpdate({house: req.body.houseId}, {house: undefined} , (err, ) => {
    if (err){
      res.status(500).send({message: err});
      return
    }
  })
     await Note.deleteOne({house: req.body.houseId}).then(() => {
    if (!req.body._id){
      res.status(404).send({message: "Recado não encontrado."});
      return;
    }
   
   })  
    await PantryItem.deleteOne({house: req.body.houseId}).then(() => {
      if (!req.body._id){
        res.status(404).send({message: "Item não encontrado."});
        return;
      }
    
    })
    await ShopList.deleteOne({house: req.body.houseId}).then(() => {
      if (!req.body._id){
        res.status(404).send({message: "Lista não encontrada."});
        return;
      }
    return;
    })
  }).catch((err) => {
    if (err) {res.status(500).send({message: err})
    return;
  }
})
  res.status(202).send({message: 'Casa deletada com sucesso!'})
  return
}


