import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config();

const ShopList = db.shoplist

const House = db.house


export const createShopList = (req,res) => {

  const lastUpdate = new Date();


  const shopList = new ShopList({
      name: req.body.name,
      lastUpdate,
      shopListItems: req.body.shopListItems,
      house: req.body.houseId
  })

  const {name, shopListItems}  = req.body
  if (!name){
    return res.status(400).send({message: 'Lista não criada, um nome é necessário.'})
  }
  if (!shopListItems.length){
    return res.status(400).send({message: 'Lista não criada, não é possível fazer a criação da lista sem itens.'})
  }

    shopList.save(async (err, item) => {
    if (err) {
      res.status(505).send({ message: err });
      return;
    }
    House.findByIdAndUpdate({_id: req.body.houseId}, {$push: {shopLists: shopList}} , (err, house) => {
      if (err){
        res.status(500).send({message: err});
        return
      }
      return;
    })
  });
    
    res.status(201).send({message: 'Lista criada com sucesso!', data: shopList})
}

export const getAll = async(req,res) => {
  ShopList.find({}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const getById = async(req,res) => {
  const {_id} = req.query

  ShopList.findById({_id}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const updateById = (req,res) => {
  const {_id, changes} = req.body
  if (!_id){
    res.status(404).send({message: "Lista não encontrada."});
    return;
  }
  if (!Object.keys(changes).length){
    res.status(404).send({message: "Erro, faltam propriedades para atualizar a lista."});
    return;
  }
  ShopList.findByIdAndUpdate({_id}, {$set: {...changes}}, (err, doc) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
      res.status(201).send({message: 'Lista atualizada com sucesso!'})
      return;
    })
  
}

export const getHouseShopLists = async(req,res) => {
  const {houseId} = req.query

  if (houseId){
      House.findById({_id: houseId}).populate("shopLists").exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
    if (!house){
      res.status(404).send({message: 'Casa não encontrada!'})
      return
    }

    if (!house.shopLists.length){
      res.status(200).send({message: 'Não existem listas criadas neste momento!'})
      return;
    }
        res.status(200).json(house.shopLists)
        return
      })
      return
}
    res.status(404).send({message: 'Ocorreu um problema e a casa não pode ser encontrada.'})
}

export const deleteById = async(req,res) => {
  await ShopList.deleteOne({_id: req.body._id}).then(() => {
    if (!req.body._id){
      res.status(404).send({message: "Lista não encontrada."});
      return;
   }
   House.findByIdAndUpdate({_id: req.body.houseId}, {$pull: {shopLists: req.body._id}} , (err, ) => {
    if (err){
      res.status(500).send({message: err});
      return
    }
    return;
  })
  }).catch((err) => {
    if (err) {res.status(500).send({message: err})
    return;
  }
})
  res.status(202).send({message: 'Lista deletada com sucesso!'})
  return
}



