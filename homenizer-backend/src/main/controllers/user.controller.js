import dotenv from "dotenv";
import db from '../models/index.js'
import bcrypt from 'bcrypt'

dotenv.config();

const User = db.user

const Role = db.role

const Note = db.note

const PantryItem = db.pantryitem

const ShopList = db.shoplist

const House = db.house

export const getAll = async(req,res) => {
  User.find({}).populate("roles", "-__v").exec((err, result) => {
    if (err) {res.status(500).send({message: err})
    return;
  }
  res.status(200).json(result)
  })
  return
}


export const getById = async(req,res) => {
  const {_id} = req.query

  User.findById({_id}).populate("roles", "-__v").exec((err,result) => {
      if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const deleteById = async(req,res) => {

  await User.deleteOne({_id: req.body.id}).exec((err,result) => {
    if (!req.body.id){
      return res.status(404).send({message: "Usuário não encontrado."})
    }
      if (err) {res.status(500).send({message: err})
      return;
    }
  })
  res.status(202).send({message: 'Usuário deletado com sucesso!'})

  return
}

export const createUser = (req,res) => {
  const user = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
  })
  user.save((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if (req.body.roles) {
        Role.find(
          {
            name: { $in: req.body.roles }
          },
          (err, roles) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            user.roles = roles.map(role => role._id);
            user.save(err => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }
            res.status(200).send({ message: "Usuário registrado com sucesso!", data: user});
              return;
            });
            return;
          }
        );
      } else {
        Role.findOne({ name: "user" }, (err, role) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          user.roles = [role._id];
          user.save(err => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            res.status(200).send({ message: "Usuário registrado com sucesso!", data: user});
            return;
          });
          return;
        });
        return;
      }
      return;
    });
}

export const updateById = async(req,res) => {
  const {_id, changes} = req.body

  const changeRolesNameToId = async(roleName) => {
    const changeable = await Role.find({name: { $in: roleName}})
    return changeable
  }

  if (!_id){
    res.status(404).send({message: "Usuário não encontrado."});
    return;
  }
  if (!Object.keys(changes).length){
    res.status(404).send({message: "Erro, faltam propriedades para atualizar."});
    return;
  }

  if (changes.roles){
    const roleName =  changes.roles
    const treatedChanges = {...changes}
    treatedChanges.roles = await changeRolesNameToId(roleName)
    
    //clean the user roles
    User.findByIdAndUpdate({_id}, {$set: {roles: []}}, (err, doc) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
    })

    //Then insert the new role
    User.findByIdAndUpdate({_id}, { ...treatedChanges}, (err, user) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
      res.status(201).send({message: 'Dados do usuário atualizados com sucesso!', user})
      return;
    })

  }
    else {    
      User.findByIdAndUpdate({_id}, {...changes}, (err, user) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
      res.status(201).send({message: 'Dados do usuário atualizados com sucesso!', user})
      return;
    })}
  
  return;
}

export const userJoinHouse = async(req, res) => {

  const {houseId, userId} = req.body

  if (!houseId){
    res.status(404).send({message: 'A casa que você está tentando entrar, não foi encontrada.'})
    return;
  }

  if (!userId){
    res.status(404).send({message: 'Houve um problema com a autorização, por favor faça login novamente.'})
    return;
  }

  await House.findByIdAndUpdate({_id: houseId}, {$push: {users: userId}})

  
  await User.findByIdAndUpdate({_id: userId}, {house: houseId})


  res.status(200).send({message: 'Solicitação de entrada na casa, aprovada!'})
}

export const getHouseUsers = async(req,res) => {
  const {houseId, favorite, role} = req.query

  if (houseId){
    if (favorite){
      House.findById({_id: houseId}).populate('users').populate([
        {
        path: 'users',
        model: 'User'
        }, 
        {
        path: 'users',
        populate: {
          path: 'roles'
        }
      }]
      ).exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
        const result = house.users.filter((user) => user.favorite === true)
        res.status(200).json(result)
        return
      })
    }  else if(role) {
      User.find()


      House.findById({_id: houseId}).populate('users').populate([
        {
        path: 'users',
        model: 'User'
        }, 
        {
        path: 'users',
        populate: {
          path: 'roles'
        }
      }]
      ).exec(async (err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
        let userList = [];
        await house.users.forEach((user) => {
          return user.roles.forEach((userRole) => {
          if (userRole.name === role) userList.push(user) 
        })}
        )
       
        res.status(200).json(userList)
        return
      })
    } else {
      House.findById({_id: houseId}).populate('users').populate([
        {
        path: 'users',
        model: 'User'
        }, 
        {
        path: 'users',
        populate: {
          path: 'roles'
        }
      }]
      ).exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
    if (!house){
      res.status(404).send({message: 'Casa não encontrada!'})
      return
    }

    if (!house.users.length){
      res.status(200).send({message: 'Não existem usuários nesta casa ainda!'})
      return;
    }
        res.status(200).json(house.users)
        return
      })
      return
    }
    return
}
    res.status(404).send({message: 'Ocorreu um problema e a casa não pode ser encontrada.'})
}

export const removeUserFromHouse = async(req, res) => {

  const {houseId, userId} = req.body

  if (!houseId){
    res.status(404).send({message: 'Esta casa não foi encontrada.'})
    return;
  }

  if (!userId){
    res.status(404).send({message: 'Houve um problema com a identificação do usuário, por favor faça login novamente.'})
    return;
  }

  await House.findByIdAndUpdate({_id: houseId}, {$pull: {users: userId}}).then(async () => {
    
   const  searchedHouse = await House.findById({_id: houseId})

    if (searchedHouse && !searchedHouse.users.length){
      await House.deleteOne({_id: houseId}).exec(async(err, house) => {
        if (!houseId){
          res.status(404).send({message: "Casa não encontrada."});
          return;
       }
        await Note.deleteOne({house: req.body.houseId})
        await PantryItem.deleteOne({house: req.body.houseId})
        await ShopList.deleteOne({house: req.body.houseId})
      })
    }
  }
)
  await User.findByIdAndUpdate({_id: userId}, {$unset: {house: '', roles: []}}).then(() => {return res.status(200).send({message: 'Usuário removido com sucesso!'})})
}