import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config();
const User = db.user

const House = db.house

export const signIn = (req, res) => {
    User.findOne({
      email: req.body.email
    })
      .populate("roles", "-__v")
      .exec(async(err, user) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        if (!user) {
          return res.status(404).send({ message: "Usuário não encontrado." });
        }
        var passwordIsValid = bcrypt.compareSync(
          req.body.password,
          user.password
        );
        if (!passwordIsValid) {
          return res.status(401).send({
            accessToken: null,
            message: "Senha inválida!"
          });
        }
        var token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET_KEY, {
          expiresIn: 86400 // 24 hours
        });
        var authorities = [];
        for (let i = 0; i < user.roles.length; i++) {
          authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
        }

        if (user.house){
          const house = await House.findById({_id: user.house})
        res.status(200).send({
          user: {
            _id: user._id,
          username: user.username,
          email: user.email,
          roles: authorities,
          accessToken: token,
          house: user.house
          },
          house: house
        })
      return
      }
      res.status(200).send({
        user: {
          _id: user._id,
        username: user.username,
        email: user.email,
        roles: authorities,
        accessToken: token,
        house: user.house
        },
      })
      return
      });
  };

