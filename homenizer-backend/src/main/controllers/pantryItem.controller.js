import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config();

const PantryItem = db.pantryitem

const House = db.house


export const createPantryItem = (req,res) => {
  const pantryItem = new PantryItem({
      name: req.body.name,
      quantity: req.body.quantity,
      house: req.body.houseId
  })

  const {name, quantity} = req.body
  if (!name){
    return res.status(400).send({message: 'Item não criado, um nome é necessário.'})
  }
    if (!quantity){
      return res.status(400).send({message: 'Item não criado, quantidade é necessária.'})
    }


  pantryItem.save(async (err, item) => {
    if (err) {
      res.status(505).send({ message: err });
      return;
    }
    House.findByIdAndUpdate({_id: req.body.houseId}, {$push: {pantryItems: pantryItem}} , (err, house) => {
      if (err){
        res.status(500).send({message: err});
        return
      }
      return;
    })
  });
    
    res.status(201).send({message: 'Item criado com sucesso!', data: pantryItem})
}

export const getAll = async(req,res) => {
  PantryItem.find({}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const getById = async(req,res) => {
  const {_id} = req.query

  PantryItem.findById({_id}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const updateById = (req,res) => {
  const {_id, changes} = req.body
  if (!_id){
    res.status(404).send({message: "Item não encontrado."});
    return;
  }
  if (!Object.keys(changes).length){
    res.status(404).send({message: "Erro, faltam propriedades para atualizar."});
    return;
  }
      PantryItem.findByIdAndUpdate({_id}, {$set: {...changes}}, (err, doc) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
      res.status(201).send({message: 'Item atualizado com sucesso!'})
      return;
    })
  
}

export const getHousePantryItems = async(req,res) => {
  const {houseId} = req.query

  if (houseId){
      House.findById({_id: houseId}).populate("pantryItems").exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
    if (!house){
      res.status(404).send({message: 'Casa não encontrada!'})
      return
    }

    if (!house.pantryItems.length){
      res.status(200).send({message: 'Não existem items criados neste momento!'})
      return;
    }
        res.status(200).json(house.pantryItems)
        return
      })
      return
    return
}
    res.status(404).send({message: 'Ocorreu um problema e a casa não pode ser encontrada.'})
}

export const deleteById = async(req,res) => {
  await PantryItem.deleteOne({_id: req.body._id}).then(() => {
    if (!req.body._id){
      res.status(404).send({message: "Item não encontrado."});
      return;
   }
   House.findByIdAndUpdate({_id: req.body.houseId}, {$pull: {pantryItems: req.body._id}} , (err, ) => {
    if (err){
      res.status(500).send({message: err});
      return
    }
    return;
  })
  }).catch((err) => {
    if (err) {res.status(500).send({message: err})
    return;
  }
})
  res.status(202).send({message: 'Item deletado com sucesso!'})
  return
}



