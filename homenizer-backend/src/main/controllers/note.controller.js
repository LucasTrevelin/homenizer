import dotenv from "dotenv";
import db from '../models/index.js'

dotenv.config();

const Note = db.note

const House = db.house


export const createNote = (req,res) => {
  const note = new Note({
      message: req.body.message,
      favorite: req.body.favorite,
      author: req.body.author,
      house: req.body.houseId
  })
  const {message, author} = req.body
  if (!message){
    return res.status(400).send({message: 'Recado não criado, uma mensagem é necessária.'})
  }
    if (!author){
      return res.status(400).send({message: 'Recado não criado, autor não reconhecido.'})
    }

  note.save(async (err, note) => {
    if (err) {
      res.status(505).send({ message: err });
      return;
    }
       return;
    });

     House.findByIdAndUpdate({_id: req.body.houseId}, {$push: {notes: note}} , (err, house) => {
      if (err){
        res.status(500).send({message: err});
        return
      }
      return;
    })
    res.status(201).send({message: 'Recado criado com sucesso!', data: note})
}

export const getAll = async(req,res) => {
  const {favorite} = req.query

  if (favorite){
  Note.find({favorite}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return}
  Note.find({}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const getById = async(req,res) => {
  const {_id} = req.query

  Note.findById({_id}, function(err, result){
    if (err) {res.status(500).send({message: err})
      return;
    }
    res.status(200).json(result)
  })
  return
}

export const deleteById = async(req,res) => {
  await Note.deleteOne({_id: req.body._id}).then(() => {
    if (!req.body._id){
      res.status(404).send({message: "Recado não encontrado."});
      return;
   }
   House.findByIdAndUpdate({_id: req.body.houseId}, {$pull: {notes: req.body._id}} , (err, ) => {
    if (err){
      res.status(500).send({message: err});
      return
    }
    return;
  })
  }).catch((err) => {
    if (err) {res.status(500).send({message: err})
    return;
  }
})
  res.status(202).send({message: 'Recado deletado com sucesso!'})
  return
}

export const updateById = (req,res) => {
  const {_id, changes} = req.body
  if (!_id){
    res.status(404).send({message: "Recado não encontrado."});
    return;
  }
  if (!Object.keys(changes).length){
    res.status(404).send({message: "Erro, faltam propriedades para atualizar."});
    return;
  }
     Note.findByIdAndUpdate({_id}, {$set: {...changes}}, (err, doc) => {
      if (err){
        res.status(500).send({message: err})
        return;
      }
      res.status(201).send({message: 'Recado atualizado com sucesso!'})
      return;
    })
  
  return;
}

export const getHouseNotes = async(req,res) => {
  const {houseId, favorite} = req.query

  if (houseId){

    if (favorite){
      House.findById({_id: houseId}).populate("notes").exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
        const result = house.notes.filter((note) => note.favorite === true)
        res.status(200).json(result)
        return
      })
    } else {
      House.findById({_id: houseId}).populate("notes").exec((err, house) => {
        if (err) {res.status(500).send({message: err})
          return;
        }
    if (!house){
      res.status(404).send({message: 'Casa não encontrada!'})
      return
    }

    if (!house.notes.length){
      res.status(200).send({message: 'Não existem notas criadas neste momento!'})
      return;
    }
        res.status(200).json(house.notes)
        return
      })
      return
    }
    return
}
    res.status(404).send({message: 'Ocorreu um problema e a casa não pode ser encontrada.'})
}