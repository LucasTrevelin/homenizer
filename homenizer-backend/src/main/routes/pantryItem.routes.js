import { Router } from 'express'

import * as controller from '../controllers/pantryItem.controller.js'
import { verifyPantryItem } from '../middlewares/verifyPantryItem.js';

const router = Router();

router.post('/create', [
  verifyPantryItem.checkPantryItemDuplicity
  ],
  controller.createPantryItem
)

router.get('/all', 
  controller.getAll
)

router.get('',
  controller.getById
)

router.patch('', [verifyPantryItem.checkPositiveQuantityItemUpdate],controller.updateById);

router.get('/house-pantry-items', controller.getHousePantryItems);

router.delete('', controller.deleteById);


export {router}
