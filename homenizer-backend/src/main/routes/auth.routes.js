import { Router } from 'express'

import * as controller from '../controllers/auth.controller.js'

const router = Router();

router.post("/signin", controller.signIn);


export {router}