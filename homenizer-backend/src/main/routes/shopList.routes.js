import { Router } from 'express'
import * as controller from '../controllers/shopList.controller.js'
import { verifyShopList } from '../middlewares/verifyShopList.js';

const router = Router();

router.post('/create', [
  verifyShopList.checkShopListDuplicity
  ],
  controller.createShopList
)

router.get('/all', 
  controller.getAll
)

router.get('',
  controller.getById
)

router.patch('',controller.updateById);

router.get('/house-shop-lists', controller.getHouseShopLists);

router.delete('', controller.deleteById);


export {router}
