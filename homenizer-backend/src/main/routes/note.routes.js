import { Router } from 'express'

import * as controller from '../controllers/note.controller.js'
import { verifyNote } from '../middlewares/verifyNote.js';

const router = Router();

router.post('/create', 
    [verifyNote.checkDuplicateNote],
    controller.createNote
)

router.get('/all', 
    controller.getAll
)

router.get('/house-notes',
    controller.getHouseNotes
)

router.get('',
    controller.getById
)

router.delete('',
    controller.deleteById
)

router.patch('',
    controller.updateById
)

export {router}