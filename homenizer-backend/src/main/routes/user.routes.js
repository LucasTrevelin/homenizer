import { Router } from 'express'

import * as controller from '../controllers/user.controller.js'
import { verifyUser } from '../middlewares/verifyUser.js';

const router = Router();

router.post('/create', [
  verifyUser.checkDuplicateUsernameOrEmail,
  verifyUser.checkRolesExisted
  ],
  controller.createUser
)

router.get("/all", controller.getAll);

router.get("", controller.getById);

router.patch('', controller.updateById);

router.patch('/join-house', controller.userJoinHouse);

router.get('/house-users', controller.getHouseUsers);

router.delete('', controller.deleteById);

router.patch('/remove-from-house', controller.removeUserFromHouse);

export {router}
