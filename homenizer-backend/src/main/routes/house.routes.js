import { Router } from 'express'

import * as controller from '../controllers/house.controller.js'
import { verifyHouse } from '../middlewares/verifyHouse.js';

const router = Router();


router.post('/create', [
  verifyHouse.checkHouseDuplicity,
  ],
  controller.createHouse
)

router.get('/all', 
  controller.getAll
)

router.get('',
  controller.getHouseById
)

router.delete('',
  controller.deleteById
)


export {router}
