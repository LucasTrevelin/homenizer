import express from 'express'
import * as dotenv from 'dotenv'
import { cors  } from '../middlewares/index.js'
import { router as auth } from '../routes/auth.routes.js'
import { router as user } from '../routes/user.routes.js'
import { router as note } from '../routes/note.routes.js'
import { router as house } from '../routes/house.routes.js'
import { router as pantryItem } from '../routes/pantryItem.routes.js'
import { router as shopList } from '../routes/shopList.routes.js'
import { mongoHelper } from '../infra/db/mongodb/helpers/mongo-helper.js'

// Set up Global configuration access
dotenv.config()

const app = express()

app.use(express.json())
app.use(cors)

// routes
mongoHelper()
app.use('/api/user/', user)
app.use('/api/auth/', auth)
app.use('/api/note/', note)
app.use('/api/house/', house)
app.use('/api/pantry-item/', pantryItem)
app.use('/api/shop-list/', shopList)

export default app
