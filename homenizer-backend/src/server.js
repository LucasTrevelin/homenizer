import dotenv from "dotenv";

import app from './main/config/app.js'

// Set up Global configuration access
dotenv.config()
const port = process.env.PORT || 8080

app.listen(port, function () {
  console.log(`Servidor ativo na porta ${port}`)
})
