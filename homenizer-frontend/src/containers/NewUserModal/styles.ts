import styled from 'styled-components'

export const Container = styled.div`
height: fit-content;
width: fit-content;
display: flex;
flex-direction: column;
margin: 0 3rem;
position: relative;
overflow: hidden;
`
export const Item = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
align-items: center;
margin-bottom: 1rem;
z-index: 2;
& > span {
  color: ${({ theme }) => theme.colors.black};
  width: 100%;
  font-weight: 500;
  margin-bottom: 0.5rem;
}
`
export const ButtonsContainer = styled.div`
width: 100%;
display: flex;
justify-content: center;
align-items: center;
column-gap: 1.5rem;
margin-top: 1rem;
`

export const SpinnerContainer = styled.div<{loading: boolean}>`
position: absolute;
width: 100%;
height: 70%;
display: flex;
justify-content: center;
align-items: center;
z-index:  ${({ loading }) => loading ? 3 : 0};
`
