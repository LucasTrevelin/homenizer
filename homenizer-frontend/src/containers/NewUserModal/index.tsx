import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Button } from '../../components/Button'
import { Input } from '../../components/Input'
import ModalComponent from '../../components/Modals/Modal'
import Spinner from '../../components/Spinner'
import { theme } from '../../theme/theme'
import * as S from './styles'

interface NewUserProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
}

const NewUserModal: React.FC<NewUserProps> = ({ isOpen, setIsOpen }) => {
  const [newUser, setNewUser] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: ''
  })
  const [validation, setValidation] = useState({
    name: false,
    email: false,
    password: false,
    confirmPassword: false
  })
  const [isSaveDisabled, setIsSaveDisabled] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  const resetState = {
    name: '',
    email: '',
    password: '',
    confirmPassword: ''
  }
  const resetValidation = {
    name: false,
    email: false,
    password: false,
    confirmPassword: false
  }

  const onCloseModal = () => {
    setNewUser(resetState)
    setValidation(resetValidation)
  }

  const { name, email, confirmPassword, password } = newUser

  useEffect(() => {
    if (name && email && password && confirmPassword) setIsSaveDisabled(false)
    else setIsSaveDisabled(true)
  }, [name, email, password, confirmPassword])

  const passwordIsConfirmed = () => {
    return password === confirmPassword
  }

  const emptyPassword = () => {
    if (!confirmPassword && password) return true
    else return false
  }

  const formValidation = () => {
    if (
      name &&
      email &&
      email.match('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$') &&
      password &&
      confirmPassword &&
      passwordIsConfirmed()
    ) {
      setValidation({
        name: false,
        email: false,
        password: false,
        confirmPassword: false
      })
      return true
    } else {
      if (!email.match('[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')) {
        return setValidation({ ...validation, email: true })
      }
      if (!passwordIsConfirmed()) {
        return setValidation({ ...validation, confirmPassword: true })
      } else if (password) {
        return setValidation({ ...validation, confirmPassword: false })
      }
    }
  }
  const getValidationConfirmPasswordMessage = () => {
    if (!passwordIsConfirmed()) {
      return 'A senha e a confirmação de senha estão diferentes'
    }

    if (!confirmPassword && password) {
      return 'A confirmação de senha é obrigatória'
    }
  }

  return (
    <ModalComponent
      closeModal={() => setIsOpen(false)}
      modalIsOpen={isOpen}
      //full={true}
    >
      <div
        style={{
          zIndex: 6,
          backgroundColor: `${theme.colors.textWhite}`,
          borderRadius: '8px',
          padding: '1rem 1.83rem 1.83rem',
          display: 'flex',
          flexDirection: 'column',
          maxHeight: '600px',
          overflowY: 'scroll'
        }}
      >
        <h1>Criação de usuário</h1>
        <S.Container>
          <S.Item>
            <span>Insira seu nome</span>
            <Input
              name="Insira seu nome completo"
              required={validation.name}
              state={name}
              placeholder="Nome completo"
              setState={(event) => setNewUser({ ...newUser, name: event })}
              requiredMessage="O nome é obrigatório"
            />
          </S.Item>
          <S.Item>
            <span>Insira seu e-mail</span>
            <Input
              name="Insira seu e-mail"
              required={validation.email}
              placeholder="E-mail"
              state={email}
              setState={(event) => setNewUser({ ...newUser, email: event })}
              requiredMessage="O e-mail é obrigatório"
              type={'email'}
            />
          </S.Item>
          <S.Item>
            <span>Insira uma senha</span>
            <Input
              name="Insira uma senha"
              required={validation.password}
              placeholder="Senha"
              state={password}
              setState={(event) => setNewUser({ ...newUser, password: event })}
              requiredMessage="A senha é obrigatória"
              type={'password'}
            />
          </S.Item>
          <S.Item>
            <span>Confirme sua senha</span>
            <Input
              name="Confirme sua senha"
              required={!passwordIsConfirmed() || emptyPassword()}
              placeholder="Confirmação senha"
              requiredMessage={getValidationConfirmPasswordMessage()}
              state={confirmPassword}
              setState={(event) =>
                setNewUser({ ...newUser, confirmPassword: event })
              }
              type={'password'}
              disabled={!password}
            />
          </S.Item>

          <S.ButtonsContainer>
            <Button
              variant="unfilled"
              title="Cancelar"
              width="150px"
              onClick={() => {
                onCloseModal()
                setIsOpen(false)
              }}
            />
            <Button
              variant="filled"
              title="Salvar"
              width="150px"
              onClick={() => {
                setIsLoading(true)

                if (formValidation()) {
                  setTimeout(() => {
                    setIsLoading(false)
                    toast.success('Usuário criado com sucesso!')
                    onCloseModal()
                  }, 2000)
                } else {
                  toast.error(
                    'Algo deu errado na criação de usuário, por favor confira o formulário!'
                  )
                  setIsLoading(false)
                }
              }}
              disabled={isSaveDisabled || isLoading}
            />
          </S.ButtonsContainer>
          <S.SpinnerContainer loading={isLoading}>
            <Spinner isLoading={isLoading} />
          </S.SpinnerContainer>
        </S.Container>
      </div>
    </ModalComponent>
  )
}

export default NewUserModal
