import { AxiosError } from 'axios'
import React, { useState } from 'react'
import { toast } from 'react-toastify'
import AvatarProfile from '../../components/AvatarProfile'
import { Button } from '../../components/Button'
import { Input } from '../../components/Input'
import ModalComponent from '../../components/Modals/Modal'
import { IHouseResponse } from '../../models/House.model'
import { IAuthUser, IUserResponse } from '../../models/User.model'
import { removeHouserUser, updateUserById } from '../../services/User.service'
import ConfirmationModal from '../ConfirmationModal'
import * as S from './styles'

interface NewHomeProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
  user: IUserResponse
  refetch: () => void
}

const ModalUserDetails: React.FC<NewHomeProps> = ({
  isOpen,
  setIsOpen,
  user,
  refetch
}) => {
  const residentHasAdminRole = (user: IUserResponse) => {
    return user.roles.some((role) => role.name === 'admin')
  }

  const userHasAdminRole = (user: IAuthUser) => {
    let hasAdminRole: number = 0
    if (!user.roles) return false
    user.roles.some((role) => {
      return role === 'ROLE_ADMIN' ? (hasAdminRole += 1) : 0
    })
    return !!hasAdminRole
  }

  const [username, setUsername] = useState(user.username)
  const [email, setEmail] = useState(user.email)
  const [isAdmin, setIsAdmin] = useState(residentHasAdminRole(user))
  const [removeUserFromHouse, setRemoveUserFromHouse] = useState(false)

  const savedUser = JSON.parse(
    localStorage.getItem('homenizer-app')!
  ) as IAuthUser

  const savedHouse = JSON.parse(
    localStorage.getItem('user-house')!
  ) as IHouseResponse

  const closeModal = () => {
    setIsOpen(false)
    setUsername('')
    setEmail('')
    setIsAdmin(false)
  }

  const isSaveDisabled = () => {
    return !username || !email
  }

  const handleResidentEdition = async () => {
    if (userHasAdminRole(savedUser)) {
      try {
        const response = await updateUserById({
          _id: user._id,
          changes: {
            username,
            email,
            roles: isAdmin ? 'admin' : 'user'
          }
        })
        toast.success(response.data.message)
        setIsOpen(false)
        refetch()
      } catch (error) {
        return toast.error(
          (error as AxiosError).message === 'Network Error'
            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
            : (
                (error as AxiosError).response?.data as {
                  message: string
                }
              ).message
        )
      }
    }
  }

  if (isOpen)
    return (
      <ModalComponent closeModal={() => closeModal()} modalIsOpen={isOpen}>
        <S.ModalContainer>
          <h1>Detalhes</h1>
          <S.BodyContainer>
            <S.NameContainer>
              <AvatarProfile type="large" userName={username} />
              <S.InputContainer>
                <label>Nome completo</label>
                <Input
                  placeholder="Nome"
                  state={username}
                  setState={setUsername}
                  name="name"
                  type="text"
                  width="288px!important"
                  disabled={true}
                />
              </S.InputContainer>
            </S.NameContainer>
            <S.InputContainer>
              <label>E-mail</label>
              <Input
                placeholder="E-mail"
                state={email}
                setState={setEmail}
                name="email"
                type="email"
                disabled={true}
              />
            </S.InputContainer>
            <S.AdminContainer>
              <label>Dar acesso como administrador?</label>
              <S.CheckboxesContainer>
                <S.OptionContainer>
                  <S.InputCheckbox
                    disabled={!userHasAdminRole(savedUser)}
                    onClick={async () => setIsAdmin(false)}
                  >
                    <S.InputCheckMark isSelected={!isAdmin} />
                  </S.InputCheckbox>
                  <p>Não</p>
                </S.OptionContainer>
                <S.OptionContainer>
                  <S.InputCheckbox
                    disabled={!userHasAdminRole(savedUser)}
                    onClick={async () => setIsAdmin(true)}
                  >
                    <S.InputCheckMark isSelected={isAdmin} />
                  </S.InputCheckbox>
                  <p>Sim</p>
                </S.OptionContainer>
              </S.CheckboxesContainer>
            </S.AdminContainer>
          </S.BodyContainer>

          <S.ButtonsContainer>
            <Button
              variant="unfilled"
              title="Cancelar"
              width="150px"
              onClick={() => setIsOpen(false)}
            />

            <Button
              variant="filled"
              title="Salvar"
              width="150px"
              onClick={() => handleResidentEdition()}
              disabled={!userHasAdminRole(savedUser)}
            />
            <Button
              variant="danger"
              title="Remover usuário"
              width="150px"
              onClick={() => setRemoveUserFromHouse(true)}
              disabled={!userHasAdminRole(savedUser)}
            />
            <ConfirmationModal
              isOpen={removeUserFromHouse}
              onCloseModal={() => setRemoveUserFromHouse(false)}
              onConfirm={async () => {
                const response = await removeHouserUser({
                  houseId: savedHouse._id,
                  userId: user._id
                })
                refetch()
                toast.success(response.data.message)
                setRemoveUserFromHouse(false)
                setIsOpen(false)
              }}
            />
          </S.ButtonsContainer>
        </S.ModalContainer>
      </ModalComponent>
    )
  else return <></>
}

export default ModalUserDetails
