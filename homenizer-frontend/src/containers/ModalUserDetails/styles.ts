import styled from 'styled-components'

export const Container = styled.div`
  height: fit-content;
  width: fit-content;
`

export const ModalContainer = styled.div`
  z-index: 6;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 8px;
  padding: 1rem 1.83rem 1.83rem;
  display: flex;
  flex-direction: column;
  max-height: 600px;
  overflow-y: scroll;
`

export const UserListContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  margin-bottom: 2rem;
  column-gap: 1rem;
  min-width: 31.38rem;
`
export const UserOptionContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin: 0.5rem 0;
  column-gap: 2rem;
`

export const UserContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  height: 100%;
  column-gap: 1rem;
`

export const ActionContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  row-gap: 1rem;

  button {
    border: none;
    background-color: transparent;
    color: ${({ theme }) => theme.colors.tertiary};

    :hover {
      color: ${({ theme }) => theme.colors.black};
      text-decoration: underline;
    }
  }
`

export const BodyContainer = styled.div`
  margin-bottom: 2rem;
  margin: 2rem 2rem 0 2rem;
`

export const AdminContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 2rem;
  margin: 0 2rem 2rem 2rem;
`

export const CheckboxesContainer = styled.div`
  display: flex;
  width: 226px;
  justify-content: space-between;
  align-items: center;
`

export const OptionContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-size: 24px;
  font-weight: 500;
  column-gap: 10px;
`
export interface ICheckbox {
  disabled?: boolean
}

export const InputCheckbox = styled.button<ICheckbox>`
  border-radius: 50%;
  height: 24px;
  aspect-ratio: 1/1;
  background-color: transparent;
  border: ${({ theme }) => `1px solid ${theme.colors.secondary}`};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  filter: ${({ disabled }) => disabled && 'brightness(0.6)'};
`

export const InputCheckMark = styled.div<{ isSelected: boolean }>`
  border-radius: 50%;
  height: 16px;
  aspect-ratio: 1/1;
  background-color: ${({ theme, isSelected }) =>
    isSelected ? `${theme.colors.secondary}` : 'transparent'};
  transition: all 0.2s ease-in-out;
`

export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 1.5rem;
`
export const NameContainer = styled.div`
  display: flex;
  align-items: flex-start;
  column-gap: 25px;
`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 1.5rem;
  row-gap: 0.5rem;
  input {
    width: 400px;
    background-color: ${({ theme }) => theme.colors.textWhite};
    border: ${({ theme }) => `1px solid ${theme.colors.black}`};
  }
`
