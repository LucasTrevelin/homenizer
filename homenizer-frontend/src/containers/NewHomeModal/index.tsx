import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import AvatarProfile from '../../components/AvatarProfile'
import { Button } from '../../components/Button'
import ModalComponent from '../../components/Modals/Modal'
import { selectInitials } from '../../helper/selectInitials.helper'
import { theme } from '../../theme/theme'
import * as S from './styles'
import { FaTrash } from 'react-icons/fa'
import { getAllUsers } from '../../services/User.service'
import { IAuthUser, IRoles, IUserResponse } from '../../models/User.model'
import { Input } from '../../components/Input'
import { createHouse } from '../../services/House.service'
import { toast } from 'react-toastify'
import { AxiosError } from 'axios'
import { IHouse, IHouseResponse } from '../../models/House.model'
import { UserContext } from '../../context/User.context'

interface NewHomeProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
}

const NewHomeModal: React.FC<NewHomeProps> = ({ isOpen, setIsOpen }) => {
  const [usersList, setUsersList] = useState<string[]>([])
  const [isSaveDisabled, setIsSaveDisabled] = useState(true)
  const [users, setUsers] = useState<IUserResponse[]>([])
  const [houseName, setHouseName] = useState('')

  const { updateUserAsAdminInfo } = useContext(UserContext)

  const savedUser: IAuthUser = localStorage.getItem('homenizer-app')
    ? JSON.parse(localStorage.getItem('homenizer-app')!)
    : ''

  const fetchUsers = useCallback(async () => {
    const response = await getAllUsers()
    if (response) setUsers(response.data)
  }, [])

  const isAdmin = (roles: IRoles[] | string[], isSavedUser?: boolean) => {
    let hasAdminRole: number = 0

    if (savedUser) {
      roles.forEach((role) => {
        return (role as string) === ('ROLE_ADMIN' as string)
          ? (hasAdminRole += 1)
          : 0
      })
    } else {
      roles.forEach((role) => {
        return (role as IRoles).name === 'admin' ? (hasAdminRole += 1) : 0
      })
    }

    return !!hasAdminRole
  }

  useEffect(() => {
    if (houseName) setIsSaveDisabled(false)
    else setIsSaveDisabled(true)
  }, [houseName, usersList])

  useEffect(() => {
    fetchUsers()
  }, [fetchUsers])

  const navigate = useNavigate()

  return (
    <ModalComponent
      closeModal={() => setIsOpen(false)}
      modalIsOpen={isOpen}
      marginTop="6rem"
    >
      <div
        style={{
          zIndex: 6,
          backgroundColor: `${theme.colors.textWhite}`,
          borderRadius: '8px',
          padding: '1rem 1.83rem 1.83rem',
          display: 'flex',
          flexDirection: 'column',
          maxHeight: '600px',
          overflowY: 'scroll'
        }}
      >
        <h1>Nova casa</h1>
        <S.InputContainer>
          <Input
            placeholder="Digite um nome para sua casa"
            state={houseName}
            setState={setHouseName}
            name="name"
            type="text"
          />
        </S.InputContainer>
        <S.Select
          onChange={(event) => {
            if (!usersList.includes(event.currentTarget.value))
              setUsersList([...usersList, event.currentTarget.value])
          }}
          disabled={!users.length}
          defaultValue={'DEFAULT'}
        >
          <option value={'DEFAULT'} disabled hidden>
            Selecionar usuários como moradores
          </option>
          {users.length &&
            users
              .filter((user) => user._id !== savedUser._id)
              .map((user) => (
                <option key={`user-${user._id}`} value={user._id}>
                  {user.username}
                </option>
              ))}
        </S.Select>
        <S.UserListContainer>
          <S.UserOptionContainer key={`user-${savedUser._id}`}>
            <S.UserContainer>
              <AvatarProfile
                type="small"
                userName={selectInitials(savedUser.username)}
              />
              <S.Input
                placeholder="User name"
                value={savedUser.username}
                disabled
              />
            </S.UserContainer>
            <S.ActionContainer>
              {
                <p
                  style={{
                    fontSize: '12px',
                    color: `${theme.colors.secondary}`,
                    marginBottom: '0',
                    padding: '5px 10px',
                    border: `1px solid ${theme.colors.secondary}`,
                    borderRadius: '10px'
                  }}
                >
                  Admin
                </p>
              }
            </S.ActionContainer>
          </S.UserOptionContainer>
          {usersList?.length
            ? usersList.map((userId) => {
                const userSelected = users.find(
                  (userFromApi) => userFromApi._id === userId
                )
                return (
                  <S.UserOptionContainer key={`user-${userId}`}>
                    <S.UserContainer>
                      <AvatarProfile
                        type="small"
                        userName={selectInitials(userSelected!.username)}
                      />
                      <S.Input
                        placeholder="User name"
                        value={userSelected!.username}
                        disabled
                      />
                    </S.UserContainer>
                    <S.ActionContainer>
                      <button
                        onClick={() => {
                          if (usersList.length > 1) {
                            const newList = usersList.filter(
                              (userFromList) => userFromList !== userId
                            )
                            setUsersList(newList)
                          } else {
                            setUsersList([])
                          }
                        }}
                      >
                        <FaTrash />
                      </button>
                      {isAdmin(userSelected!.roles) && (
                        <p
                          style={{
                            fontSize: '12px',
                            color: `${theme.colors.secondary}`,
                            marginBottom: '0',
                            padding: '5px 10px',
                            border: `1px solid ${theme.colors.secondary}`,
                            borderRadius: '10px'
                          }}
                        >
                          Admin
                        </p>
                      )}
                    </S.ActionContainer>
                  </S.UserOptionContainer>
                )
              })
            : undefined}
        </S.UserListContainer>

        <S.ButtonsContainer>
          <Button
            variant="unfilled"
            title="Cancelar"
            width="150px"
            onClick={() => setIsOpen(false)}
          />
          <Button
            variant="filled"
            title="Salvar"
            width="150px"
            onClick={async () => {
              try {
                let newUsers = usersList.filter(
                  (userId) => userId !== savedUser._id
                )

                const adminUser = { ...savedUser }
                adminUser.roles = ['admin']

                await createHouse({
                  name: houseName,
                  users: newUsers,
                  adminUser: adminUser
                }).then(async (response) => {
                  await localStorage.setItem(
                    'user-house',
                    JSON.stringify(
                      (response.data as { message: string; data: IHouse }).data
                    )
                  )
                  toast.success('Casa criada com sucesso!')

                  await updateUserAsAdminInfo(savedUser)
                  const savedHouse: IHouseResponse = JSON.parse(
                    localStorage.getItem('user-house')!
                  )
                  if (savedHouse) navigate('/home')
                })
              } catch (error) {
                return toast.error(
                  (error as AxiosError).message === 'Network Error'
                    ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                    : (
                        (error as AxiosError).response?.data as {
                          message: string
                        }
                      ).message
                )
              }
            }}
            disabled={isSaveDisabled}
          />
        </S.ButtonsContainer>
      </div>
    </ModalComponent>
  )
}

export default NewHomeModal
