import styled from 'styled-components'

export const Container = styled.div`
  height: fit-content;
  width: fit-content;
`

export const UserListContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  margin-bottom: 2rem;
  column-gap: 1rem;
  min-width: 31.38rem;
`
export const UserOptionContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin: 0.5rem 0;
  column-gap: 2rem;
`

export const UserContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  height: 100%;
  column-gap: 1rem;
`

export const ActionContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  row-gap: 1rem;

  button {
    border: none;
    background-color: transparent;
    color: ${({ theme }) => theme.colors.tertiary};

    :hover {
      color: ${({ theme }) => theme.colors.black};
      text-decoration: underline;
    }
  }
`

export const BodyContainer = styled.div`
  margin-bottom: 2rem;
  margin: 0 2rem 2rem 2rem;
`

export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 1.5rem;
`

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 1.5rem;
  row-gap: 0.5rem;
  input {
    width: 400px;
    background-color: ${({ theme }) => theme.colors.textWhite};
    border: ${({ theme }) => `1px solid ${theme.colors.black}`};
  }
`
export const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
`
