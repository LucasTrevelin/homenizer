import { AxiosError } from 'axios'
import React, { useState } from 'react'
import { toast } from 'react-toastify'
import { Button } from '../../components/Button'
import { Input } from '../../components/Input'
import ModalComponent from '../../components/Modals/Modal'
import { validateEmail } from '../../helper/validateEmail.helper'
import { IUser } from '../../models/User.model'
import { createUser } from '../../services/User.service'
import { theme } from '../../theme/theme'
import * as S from './styles'

interface NewHomeProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
}

const CreateUserModal: React.FC<NewHomeProps> = ({ isOpen, setIsOpen }) => {
  const [username, setUserName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const emailIsValid = validateEmail(email) || !email

  const isSaveDisabled = () => {
    return (
      !username ||
      !email ||
      !password ||
      !emailIsValid ||
      !confirmPassword ||
      password !== confirmPassword
    )
  }

  const handleUserCreation = async (userCredentials: IUser) => {
    try {
      const response = await createUser(userCredentials)
      if (response.status === 200) {
        toast.success(
          (response.data as { message: string }).message as unknown as string
        )
        setUserName('')
        setEmail('')
        setPassword('')
        setConfirmPassword('')
        setIsOpen(false)
      }
    } catch (error) {
      toast.error(
        (error as AxiosError).message === 'Network Error'
          ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
          : ((error as AxiosError).response?.data as { message: string })
              .message
      )
    }
  }

  return (
    <ModalComponent
      closeModal={() => {
        setIsOpen(false)
      }}
      modalIsOpen={isOpen}
    >
      <div
        style={{
          zIndex: 6,
          backgroundColor: `${theme.colors.textWhite}`,
          borderRadius: '8px',
          padding: '1rem 1.83rem 1.83rem',
          display: 'flex',
          flexDirection: 'column',
          overflowY: 'scroll'
        }}
      >
        <S.StyledForm>
          <h1>Cadastro de usuários</h1>
          <S.BodyContainer>
            <S.InputContainer>
              <label>Insira seu nome completo</label>
              <Input
                placeholder="Nome"
                state={username}
                setState={setUserName}
                name="name"
                type="text"
              />
            </S.InputContainer>
            <S.InputContainer>
              <label>Insira seu e-mail</label>
              <Input
                placeholder="E-mail"
                state={email}
                setState={setEmail}
                name="email"
                type="email"
                required={!emailIsValid}
                requiredMessage={'Insira um e-mail no formato correto!'}
              />
            </S.InputContainer>
            <S.InputContainer>
              <label>Insira sua senha</label>
              <Input
                placeholder="Senha"
                state={password}
                setState={setPassword}
                name="password"
                type="password"
              />
            </S.InputContainer>
            <S.InputContainer>
              <label>Confirme sua senha</label>
              <Input
                placeholder="Senha"
                state={confirmPassword}
                setState={setConfirmPassword}
                name="confirmPassword"
                type="password"
              />
            </S.InputContainer>
          </S.BodyContainer>

          <S.ButtonsContainer>
            <Button
              variant="filled"
              title="Salvar"
              width="150px"
              onClick={(event) => {
                event.preventDefault()
                if (emailIsValid)
                  return handleUserCreation({
                    username,
                    email,
                    password,
                    _id: ''
                  })
              }}
              disabled={isSaveDisabled()}
            />
            <Button
              variant="unfilled"
              title="Cancelar"
              width="150px"
              onClick={(event) => {
                event.preventDefault()
                setUserName('')
                setEmail('')
                setPassword('')
                setConfirmPassword('')
                setIsOpen(false)
              }}
            />
          </S.ButtonsContainer>
        </S.StyledForm>
      </div>
    </ModalComponent>
  )
}

export default CreateUserModal
