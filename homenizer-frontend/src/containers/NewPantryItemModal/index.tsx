import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Button } from '../../components/Button'
import { Input } from '../../components/Input'
import ModalComponent from '../../components/Modals/Modal'
import Spinner from '../../components/Spinner'
import { IHouseResponse } from '../../models/House.model'
import { IPantryItem, IPantryItemRequest } from '../../models/PantryItem.model'
import * as S from './styles'

interface NewUserProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
  onSave: (
    newItem: IPantryItemRequest,
    onCloseModal: () => void,
    setLoadingModalState: (isLoading: boolean) => void
  ) => void
}

const NewPantryItemModal: React.FC<NewUserProps> = ({
  isOpen,
  setIsOpen,
  onSave
}) => {
  const [newPantryItem, setNewPantryItem] = useState<IPantryItem>({
    name: '',
    quantity: 0
  })

  const [isSaveDisabled, setIsSaveDisabled] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  const resetState: IPantryItem = {
    name: '',
    quantity: 0
  }

  const savedHouse: IHouseResponse = JSON.parse(
    localStorage.getItem('user-house')!
  )

  const onCloseModal = () => {
    setNewPantryItem(resetState)
    setIsOpen(false)
  }

  const { name, quantity } = newPantryItem

  const onlyNumbers = (arg: string) => arg.replace(/\D/g, '')

  useEffect(() => {
    if (name && quantity) setIsSaveDisabled(false)
    else setIsSaveDisabled(true)
  }, [name, quantity])

  return (
    <ModalComponent closeModal={() => setIsOpen(false)} modalIsOpen={isOpen}>
      <S.InformationContainer>
        <h1>Criação de Item do Despensa</h1>
        <S.Container>
          <S.Item>
            <span>Insira o nome do item</span>
            <Input
              name="Insira o nome do item"
              state={newPantryItem.name}
              placeholder="Nome do item"
              setState={(event) =>
                setNewPantryItem({ ...newPantryItem, name: event })
              }
            />
          </S.Item>
          <S.Item>
            <span>Insira a quantidade (somente unidades inteiras)</span>
            <Input
              name="Insira a quantidadel"
              placeholder="Quantidade"
              state={String(newPantryItem.quantity)}
              setState={(event) => {
                setNewPantryItem({
                  ...newPantryItem,
                  quantity: Number(onlyNumbers(event))
                })
              }}
              type={'number'}
            />
          </S.Item>

          <S.ButtonsContainer>
            <Button
              variant="unfilled"
              title="Cancelar"
              width="150px"
              onClick={() => {
                onCloseModal()
                setIsOpen(false)
                setIsLoading(false)
              }}
            />
            <Button
              variant="filled"
              title="Salvar"
              width="150px"
              onClick={async () => {
                setIsLoading(true)
                await onSave(
                  {
                    name,
                    quantity,
                    houseId: savedHouse._id
                  },
                  onCloseModal,
                  setIsLoading
                )
                setIsLoading(false)
              }}
              disabled={isSaveDisabled || isLoading}
            />
          </S.ButtonsContainer>
          <S.SpinnerContainer loading={isLoading}>
            <Spinner isLoading={isLoading} />
          </S.SpinnerContainer>
        </S.Container>
      </S.InformationContainer>
    </ModalComponent>
  )
}

export default NewPantryItemModal
