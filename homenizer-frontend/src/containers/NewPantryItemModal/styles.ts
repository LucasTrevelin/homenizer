import styled from 'styled-components'

export const Container = styled.div`
  height: fit-content;
  width: fit-content;
  display: flex;
  flex-direction: column;
  margin: 0 3rem;
  position: relative;
  overflow: hidden;
`

export const InformationContainer = styled.div`
  z-index: 99 !important;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 8px;
  padding: 1rem 1.83rem 1.83rem;
  display: flex;
  flex-direction: column;
  max-height: 600px;
  overflow-y: auto;
`
export const Item = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  margin-bottom: 1rem;
  z-index: 2;
  & > span {
    color: ${({ theme }) => theme.colors.black};
    width: 100%;
    font-weight: 500;
    margin-bottom: 0.5rem;
  }
`
export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 1.5rem;
  margin-top: 1rem;
`

export const SpinnerContainer = styled.div<{ loading: boolean }>`
  position: absolute;
  width: 100%;
  height: 70%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: ${({ loading }) => (loading ? 3 : 0)};
`
