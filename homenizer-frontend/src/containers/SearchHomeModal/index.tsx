import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import AvatarProfile from '../../components/AvatarProfile'
import { Button } from '../../components/Button'
import ModalComponent from '../../components/Modals/Modal'
import { selectInitials } from '../../helper/selectInitials.helper'
import { theme } from '../../theme/theme'
import * as S from './styles'
import { joinHouse } from '../../services/User.service'
import { IRoles, IUserResponse } from '../../models/User.model'
import { getHouseById } from '../../services/House.service'
import { toast } from 'react-toastify'
import { AxiosError } from 'axios'
import { IHouseResponse } from '../../models/House.model'
import Spinner from '../../components/Spinner'
import Badge from '../../components/Badge'

interface NewHomeProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
  availableHouses?: IHouseResponse[]
}

const SearchHomeModal: React.FC<NewHomeProps> = ({
  isOpen,
  setIsOpen,
  availableHouses
}) => {
  const [selectedHouseId, setSelectedHouseId] = useState<string>('')
  const [isSaveDisabled, setIsSaveDisabled] = useState(true)
  const [users, setUsers] = useState<IUserResponse[]>([])
  const [loading, setLoading] = useState(false)

  const savedUser: IUserResponse = localStorage.getItem('homenizer-app')
    ? JSON.parse(localStorage.getItem('homenizer-app')!)
    : ''

  const isAdmin = (roles: IRoles[]) => {
    let hasAdminRole: number = 0
    roles.forEach((role) => {
      return role.name === 'admin' ? (hasAdminRole += 1) : 0
    })

    return !!hasAdminRole
  }

  useEffect(() => {
    if (selectedHouseId) setIsSaveDisabled(false)
    else setIsSaveDisabled(true)
  }, [selectedHouseId])

  const navigate = useNavigate()

  return (
    <>
      <Spinner isLoading={loading} />
      <ModalComponent
        closeModal={() => setIsOpen(false)}
        modalIsOpen={isOpen}
        marginTop="6rem"
      >
        <div
          style={{
            zIndex: 6,
            backgroundColor: `${theme.colors.textWhite}`,
            borderRadius: '8px',
            padding: '1rem 1.83rem 1.83rem',
            display: 'flex',
            flexDirection: 'column',
            maxHeight: '600px',
            overflowY: 'scroll'
          }}
        >
          <h1>Selecione uma casa</h1>
          <S.Select
            onChange={async (event) => {
              setSelectedHouseId(event.currentTarget.value)
              const houseId = availableHouses?.find(
                (house) => house._id === event.currentTarget.value
              )?._id
              if (houseId) {
                setLoading(true)
                const response = await getHouseById(houseId)
                setLoading(false)
                setUsers(response.data.users as IUserResponse[])
              }
            }}
            defaultValue={'DEFAULT'}
          >
            <option value={'DEFAULT'} disabled hidden>
              Selecione uma casa para entrar
            </option>
            {availableHouses?.length &&
              availableHouses.map((house) => (
                <option key={`user-${house._id}`} value={house._id}>
                  {house.name}
                </option>
              ))}
          </S.Select>
          <S.UserListContainer>
            {users?.length
              ? users.map((user) => {
                  return (
                    <S.UserOptionContainer key={`user-${user}`}>
                      <S.UserContainer>
                        <AvatarProfile
                          type="small"
                          userName={selectInitials(user!.username)}
                        />
                        <S.Input
                          placeholder="User name"
                          value={user!.username}
                          disabled
                        />
                      </S.UserContainer>
                      <S.ActionContainer>
                        {isAdmin(user!.roles) && <Badge label="Admin" />}
                      </S.ActionContainer>
                    </S.UserOptionContainer>
                  )
                })
              : undefined}
          </S.UserListContainer>

          <S.ButtonsContainer>
            <Button
              variant="unfilled"
              title="Cancelar"
              width="150px"
              onClick={() => setIsOpen(false)}
            />
            <Button
              variant="filled"
              title="Salvar"
              width="150px"
              onClick={async () => {
                try {
                  const response = await joinHouse({
                    houseId: selectedHouseId,
                    userId: savedUser._id
                  })
                  const selectedHouse = availableHouses?.find(
                    (house) => house._id === selectedHouseId
                  )
                  await localStorage.setItem(
                    'user-house',
                    JSON.stringify(selectedHouse)
                  )
                  toast.success(response.data.message)
                  navigate('/home')
                } catch (error) {
                  return toast.error(
                    (error as AxiosError).message === 'Network Error'
                      ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                      : (
                          (error as AxiosError).response?.data as {
                            message: string
                          }
                        ).message
                  )
                }
              }}
              disabled={isSaveDisabled}
            />
          </S.ButtonsContainer>
        </div>
      </ModalComponent>
    </>
  )
}

export default SearchHomeModal
