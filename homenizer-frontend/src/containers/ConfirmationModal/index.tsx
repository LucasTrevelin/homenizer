import React from 'react'
import { Button } from '../../components/Button'
import ModalComponent from '../../components/Modals/Modal'
import { theme } from '../../theme/theme'
import * as S from './styles'

interface IConfirmantionModal {
  onConfirm: () => void
  onCloseModal: () => void
  isOpen: boolean
}

const ConfirmationModal: React.FC<IConfirmantionModal> = ({
  onCloseModal,
  onConfirm,
  isOpen
}) => {
  return (
    <ModalComponent closeModal={() => onCloseModal()} modalIsOpen={isOpen}>
      <div
        style={{
          zIndex: 6,
          backgroundColor: `${theme.colors.textWhite}`,
          borderRadius: '8px',
          padding: '1rem 1.83rem 1.83rem',
          display: 'flex',
          flexDirection: 'column',
          maxHeight: '600px',
          overflowY: 'scroll'
        }}
      >
        <h1>Cadastro de usuários</h1>
        <S.BodyContainer>
          <S.InputContainer>
            <label>Deseja mesmo remover este usuário da casa?</label>
          </S.InputContainer>
        </S.BodyContainer>

        <S.ButtonsContainer>
          <Button
            variant="unfilled"
            title="Não"
            width="150px"
            onClick={() => onCloseModal()}
          />
          <Button
            variant="danger"
            title="Sim"
            width="150px"
            onClick={
              () => onConfirm()
              //handleUserCreation({ username, email, password, _id: '' })
            }
            //disabled={isSaveDisabled()}
          />
        </S.ButtonsContainer>
      </div>
    </ModalComponent>
  )
}

export default ConfirmationModal
