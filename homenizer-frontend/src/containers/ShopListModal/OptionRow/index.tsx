import React from 'react'
import { Input } from '../../../components/Input'
import { IShopListItemCreate } from '../../../models/ShopList.model'

import * as S from './styles'

interface IOptionRow {
  setShopListItems: (shopListItem: IShopListItemCreate[]) => void
  shopListItems?: IShopListItemCreate[]
  onRemoveItem: (index: number) => void
  onEditQuantity: (index: number, type: 'subtract' | 'add') => void
  itemId?: number
}

const OptionRow: React.FC<IOptionRow> = ({
  setShopListItems,
  shopListItems,
  onRemoveItem,
  onEditQuantity,
  itemId
}) => {
  if (itemId) {
    const itemIdIndex = itemId - 1
    return (
      <S.OptionListContainer>
        <S.InputCheckbox
          onClick={() => {
            if (shopListItems) {
              const editedItems = [...shopListItems]
              const checkedStatus = editedItems[itemIdIndex].checked
              editedItems[itemIdIndex].checked = !checkedStatus

              setShopListItems(editedItems)
            }
          }}
        >
          {shopListItems?.[itemIdIndex] && (
            <S.InputCheckMark isSelected={shopListItems[itemIdIndex].checked} />
          )}
        </S.InputCheckbox>
        <Input
          name="item option"
          type="unfilled"
          placeholder="Escreva o nome do item"
          fontSize="16px"
          setState={(event) => {
            if (shopListItems) {
              const editedItems = [...shopListItems]
              editedItems[itemIdIndex] = {
                ...editedItems[itemIdIndex],
                name: event
              }
              setShopListItems(editedItems)
            }
          }}
          width="250px"
          borderRadius="0"
          state={shopListItems?.[itemIdIndex]?.name}
        />
        <Input
          name="item quantity"
          type="square"
          fontSize="16px"
          margin="0 1rem"
          disabled
          setState={(event) => {
            if (shopListItems) {
              const editedItems = [...shopListItems]
              editedItems[itemIdIndex] = {
                ...editedItems[itemIdIndex],
                name: event
              }
              setShopListItems(editedItems)
            }
          }}
          width="25px"
          borderRadius="0"
          state={String(shopListItems?.[itemIdIndex]?.quantity)}
        />
        <S.ActionItemContainer
          centralize={!shopListItems?.[itemIdIndex]?.quantity}
        >
          <S.IconButton onClick={() => onEditQuantity(itemIdIndex, 'add')}>
            <img
              alt="add-one-quantity-icon"
              src="/assets/icons/plus-button-icon.png"
            />
          </S.IconButton>
          {shopListItems?.[itemIdIndex]?.quantity ? (
            <S.IconButton
              onClick={() => onEditQuantity(itemIdIndex, 'subtract')}
            >
              <img
                alt="remove-one-quantity-icon"
                src="/assets/icons/remove-button-icon.png"
              />
            </S.IconButton>
          ) : (
            <></>
          )}
        </S.ActionItemContainer>
        <S.ActionItemContainer>
          <S.IconButton
            onClick={() => onRemoveItem(itemIdIndex)}
            marginLeft="1.5rem"
          >
            <S.RemoveRowButtonImg
              alt="remove-row-icon"
              src="/assets/icons/trash-icon.png"
            />
          </S.IconButton>
        </S.ActionItemContainer>
      </S.OptionListContainer>
    )
  }
  return <></>
}

export default OptionRow
