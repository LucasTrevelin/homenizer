import styled from 'styled-components'
import { ICheckbox } from '../../ModalUserDetails/styles'

export const InputCheckbox = styled.button<ICheckbox>`
  border-radius: 50%;
  height: 42px;
  aspect-ratio: 1/1;
  background-color: transparent;
  border: ${({ theme }) => `1px solid ${theme.colors.secondary}`};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  filter: ${({ disabled }) => disabled && 'brightness(0.6)'};
  z-index: 2;
  margin-bottom: 0.5rem;
`

export const InputCheckMark = styled.div<{ isSelected: boolean }>`
  border-radius: 50%;
  height: 30px;
  aspect-ratio: 1/1;
  background-color: ${({ theme, isSelected }) =>
    isSelected ? `${theme.colors.secondary}` : 'transparent'};
  transition: all 0.2s ease-in-out;
`
export const ActionItemContainer = styled.div<{ centralize?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 0.3rem;
  height: 100%;
  padding-bottom: 10px;
  min-width: ${({ centralize }) => centralize && '108px'};
`

export const IconButton = styled.button<{
  marginLeft?: string
}>`
  aspect-ratio: 1/1;
  min-width: 50px;
  padding: 1;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 50%;
  border: transparent;
  outline: none;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 4;
  transition: all 0.2s ease-in-out;
  margin-left: ${({ marginLeft }) => marginLeft && marginLeft};

  :hover {
    transition: all 0.2s ease-in-out;
    cursor: pointer;
    filter: brightness(0.9);
  }
`
export const OptionListContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  height: 100%;
  & > span {
    font-size: 14px;
    color: ${({ theme }) => theme.colors.tertiary};
    font-weight: bold;
  }
`

export const RemoveRowButtonImg = styled.img`
  aspect-ratio: 1/1;
  width: 36px;
  padding-bottom: 0.2rem;
`
