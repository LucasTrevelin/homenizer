import { AxiosError } from 'axios'
import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Button } from '../../components/Button'
import { Input } from '../../components/Input'
import ModalComponent from '../../components/Modals/Modal'
import { IHouseResponse } from '../../models/House.model'
import {
  IShopListItemCreate,
  IShopListRequest,
  IShopListCreate,
  IShopListItem,
  IShopList,
  IUpdateShopListByIdParams
} from '../../models/ShopList.model'
import {
  createShopList,
  updateShopListById
} from '../../services/ShopList.service'
import OptionRow from './OptionRow'
import * as S from './styles'

export const enum EShopListModalType {
  NEW = 'new',
  EDIT = 'edit'
}

interface INewShopListProps {
  isOpen: boolean
  setIsOpen: (isOpen: boolean) => void
  refetch: () => Promise<void>
  type: EShopListModalType
  shopList?: IShopListCreate
}

const ShopListModal: React.FC<INewShopListProps> = ({
  isOpen,
  setIsOpen,
  refetch,
  type,
  shopList
}) => {
  const resetShopListItemsState: IShopListItemCreate[] = [
    {
      id: EShopListModalType.NEW ? 1 : 0,
      name: '',
      quantity: 0,
      checked: false
    }
  ]

  const newShopListItemsObject: IShopListItemCreate = {
    id: 0,
    name: '',
    quantity: 0,
    checked: false
  }

  const resetShopListState: IShopListCreate = {
    name: '',
    shopListItems: resetShopListItemsState
  }

  const addVirtualIdToShopListItems = (shopListItems: IShopListItem[]) => {
    const newList = shopListItems.map((item, index) => ({
      ...item,
      id: index + 1
    }))
    return newList
  }

  const [newShopListItems, setNewShopListItems] = useState<
    IShopListItemCreate[]
  >(
    shopList?.shopListItems
      ? addVirtualIdToShopListItems(shopList?.shopListItems)
      : resetShopListItemsState
  )

  const [newShopList, setNewShopList] = useState<IShopListCreate>(
    shopList
      ? {
          ...shopList!,
          shopListItems: addVirtualIdToShopListItems(shopList!.shopListItems)
        }
      : resetShopListState
  )

  const [isSaveDisabled, setIsSaveDisabled] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  const savedHouse: IHouseResponse = JSON.parse(
    localStorage.getItem('user-house')!
  )

  const onCloseModal = async () => {
    await refetch()
    setNewShopList(resetShopListState)
    setNewShopListItems(resetShopListItemsState)
    setIsOpen(false)
  }

  const { name } = newShopList

  const OptionRowLabel = () => {
    return (
      <S.OptionListContainer>
        <S.CheckTitle>Check</S.CheckTitle>
        <S.DummyInput />
        <S.CheckTitle>Qtde.</S.CheckTitle>
        <S.DummySpace />
        <S.CheckTitle>Adc./remover</S.CheckTitle>
      </S.OptionListContainer>
    )
  }

  const handleAddItem = (index: number) => {
    const pushedShopListItem = [
      ...newShopListItems,
      { ...newShopListItemsObject, id: index + 1 }
    ]
    setNewShopListItems(pushedShopListItem)
  }

  const handleEditQuantity = (index: number, type: 'subtract' | 'add') => {
    const toBeAddedQuantity = newShopListItems[index].quantity

    const editedNewShopListItems = [...newShopListItems]

    if (type === 'add')
      editedNewShopListItems[index].quantity = toBeAddedQuantity + 1

    if (type === 'subtract')
      editedNewShopListItems[index].quantity = toBeAddedQuantity - 1

    setNewShopListItems(editedNewShopListItems)
  }

  const handleRemoveItem = (index: number) => {
    const filteredList = newShopListItems.filter((item) => {
      if (type === EShopListModalType.NEW) return item.id !== index
      else {
        return item.id !== index + 1
      }
    })

    setNewShopListItems(filteredList)
  }

  useEffect(() => {
    const invalidateAllItemsHasName = newShopListItems.find(
      (item) => !item.name || !item.quantity || item.quantity < 0
    )
    if (name && newShopListItems.length && !invalidateAllItemsHasName) {
      setIsSaveDisabled(false)
    } else {
      setIsSaveDisabled(true)
    }
  }, [name, newShopListItems])

  if (isOpen) {
    return (
      <ModalComponent closeModal={() => setIsOpen(false)} modalIsOpen={isOpen}>
        <S.InformationContainer>
          <h1>Criação de nova lista</h1>
          <S.Container>
            <S.Item>
              <Input
                name="Nome da lista"
                state={newShopList.name}
                placeholder="Escreva um título"
                borderRadius="0"
                fontSize="20px"
                padding="5px"
                setState={(event) =>
                  setNewShopList({ ...newShopList, name: event })
                }
                type="unfilled"
              />
              <S.AddRowContainer>
                <Button
                  title="Adicione um item"
                  variant="success"
                  height="40px"
                  width="132px"
                  onClick={() => handleAddItem(newShopListItems.length)}
                />
              </S.AddRowContainer>
            </S.Item>
            {newShopListItems.length ? <OptionRowLabel /> : <></>}
            {newShopListItems.map((option, index) => (
              <OptionRow
                shopListItems={newShopListItems}
                setShopListItems={setNewShopListItems}
                key={`option-shop-list${index}`}
                itemId={option.id}
                onRemoveItem={handleRemoveItem}
                onEditQuantity={handleEditQuantity}
              />
            ))}

            <S.ButtonsContainer>
              <Button
                variant="unfilled"
                title="Cancelar"
                width="150px"
                onClick={() => {
                  onCloseModal()
                  setIsOpen(false)
                }}
              />
              <Button
                variant="filled"
                title="Salvar"
                width="150px"
                onClick={async () => {
                  setIsLoading(true)
                  try {
                    const shopListItemsRequest: IShopListItemCreate[] =
                      JSON.parse(JSON.stringify(newShopListItems))
                    if (type === EShopListModalType.NEW) {
                      await shopListItemsRequest.forEach(
                        (item) => delete item.id
                      )
                      const newShopListRequest: IShopListRequest = {
                        ...newShopList,
                        shopListItems: shopListItemsRequest,
                        houseId: savedHouse._id
                      }

                      const response = await createShopList({
                        ...newShopListRequest
                      })
                      toast.success(response.data.message, {
                        toastId: 'create-shop-list-success'
                      })
                      setIsLoading(false)
                      onCloseModal()
                    } else {
                      await shopListItemsRequest.forEach(
                        (item) => delete item.id
                      )
                      if (newShopList._id) {
                        const editedShopListRequest: IUpdateShopListByIdParams =
                          {
                            _id: newShopList._id,
                            changes: {
                              ...newShopList,
                              shopListItems: shopListItemsRequest
                            }
                          }
                        const response = await updateShopListById({
                          ...editedShopListRequest
                        })
                        toast.success(response.data.message, {
                          toastId: 'update-shop-list-success'
                        })
                        setIsLoading(false)
                        onCloseModal()
                      }
                    }
                  } catch (error) {
                    setIsLoading(false)
                    toast.error(
                      (
                        (error as AxiosError).response?.data as {
                          message: string
                        }
                      ).message
                    )
                  }
                }}
                disabled={isSaveDisabled || isLoading}
              />
            </S.ButtonsContainer>
          </S.Container>
        </S.InformationContainer>
      </ModalComponent>
    )
  } else return <></>
}

export default ShopListModal
