import styled from 'styled-components'
import { ICheckbox } from '../ModalUserDetails/styles'

export const Container = styled.div`
  height: fit-content;
  width: fit-content;
  display: flex;
  flex-direction: column;
  margin: 0 3rem;
  position: relative;
  overflow: hidden;
`

export const AddRowContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const InformationContainer = styled.div`
  z-index: 99 !important;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 8px;
  padding: 1rem 1.83rem 1.83rem;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  max-height: 600px;
  overflow-y: auto;
`
export const Item = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin-bottom: 1rem;
  z-index: 2;
  min-width: 597px;
  & > input {
    margin-left: 0;
  }
`
export const OptionListContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;
  height: 100%;
`

export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 1.5rem;
  margin-top: 2rem;
`

export const SpinnerContainer = styled.div<{ loading: boolean }>`
  position: absolute;
  width: 100%;
  height: 70%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: ${({ loading }) => (loading ? 3 : 0)};
`

export const CheckTitle = styled.span`
  min-width: 42px;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.tertiary};
  font-weight: bold;
`

export const DummySpace = styled.span`
  min-width: 28.5px;
`

export const DummyInput = styled.span`
  min-width: 315.5px;
`

export const button = styled.button`
  border-radius: 50%;
  height: 42px;
  aspect-ratio: 1/1;
  background-color: transparent;
  border: ${({ theme }) => `1px solid ${theme.colors.secondary}`};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  filter: ${({ disabled }) => disabled && 'brightness(0.6)'};
  z-index: 2;
  margin-bottom: 0.5rem;
`
