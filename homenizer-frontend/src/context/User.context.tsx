import {
  ReactNode,
  createContext,
  useCallback,
  useState,
  useContext
} from 'react'
import {
  IAuthUser,
  IUserResponse,
  IUserResponseLogin
} from '../models/User.model'

interface IUserProvider {
  saveAuthInfo: (loginResponse: IUserResponseLogin) => Promise<void>
  loggedUser?: IAuthUser
  saveEditInfo: (editUserResponse: IUserResponse) => Promise<void>
  updateUserAsAdminInfo: (authUser: IAuthUser) => Promise<void>
}

const defaultValue = {} as IUserProvider

export const UserContext = createContext<{
  saveAuthInfo: (loginResponse: IUserResponseLogin) => Promise<void>
  loggedUser?: IAuthUser
  saveEditInfo: (editUserResponse: IUserResponse) => Promise<void>
  updateUserAsAdminInfo: (authUser: IAuthUser) => Promise<void>
}>(defaultValue)

export const useUserContext = () => useContext(UserContext)

export const UserProvider = ({ children }: { children: ReactNode }) => {
  const [loggedUser, setLoggedUser] = useState<IAuthUser>()

  const saveAuthInfo = useCallback(
    async (loginResponse: IUserResponseLogin) => {
      setLoggedUser(loginResponse.user)
      await localStorage.setItem(
        'homenizer-app',
        JSON.stringify(loginResponse.user)
      )
      if (loginResponse.house) {
        return localStorage.setItem(
          'user-house',
          JSON.stringify(loginResponse.house)
        )
      }
    },
    []
  )

  const saveEditInfo = useCallback(async (editUserResponse: IUserResponse) => {
    const treatEditUserResponse = { ...editUserResponse }
    treatEditUserResponse.roles.forEach((role) =>
      role ? `ROLE_${role.name.toUpperCase()}` : ''
    )
    setLoggedUser(treatEditUserResponse as unknown as IAuthUser)
    await localStorage.setItem(
      'homenizer-app',
      JSON.stringify(editUserResponse)
    )
  }, [])

  const updateUserAsAdminInfo = useCallback(async (authUser: IAuthUser) => {
    const treatedEditAuthInfo = { ...authUser }
    treatedEditAuthInfo.roles = ['ROLE_ADMIN']
    if (treatedEditAuthInfo) {
      await localStorage.setItem(
        'homenizer-app',
        JSON.stringify(treatedEditAuthInfo)
      )
      setLoggedUser(treatedEditAuthInfo as IAuthUser)
    }
  }, [])

  return (
    <UserContext.Provider
      value={{ saveAuthInfo, loggedUser, saveEditInfo, updateUserAsAdminInfo }}
    >
      {children}
    </UserContext.Provider>
  )
}
