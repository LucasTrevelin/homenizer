import React, { Suspense } from 'react'
import { Watch } from 'react-loader-spinner'
import {
  BrowserRouter,
  Routes as Switch,
  Route,
  Navigate
} from 'react-router-dom'
import { Home } from './pages/home'
import { Login } from './pages/login'
import Options from './pages/options'
import Pantry from './pages/pantry'
import HomeOptions from './pages/home-options'
import HouseResidents from './pages/residents'
import PantryOptions from './components/pages/PantryOptions'
import PrivateComponent from './components/PrivateComponent'
import UserDetails from './components/pages/UserDetails'

export const Routes: React.FC = () => {
  return (
    <Suspense
      fallback={
        <Watch height="100" width="100" color="grey" ariaLabel="loading" />
      }
    >
      <BrowserRouter>
        <Switch>
          <Route path="/login" element={<Login />} />

          <Route
            path="/home"
            element={
              <PrivateComponent userProxy houseProxy>
                <Home />
              </PrivateComponent>
            }
          />

          <Route path="/" element={<Navigate to="/login" />} />
          <Route
            path="/home/:options"
            element={
              <PrivateComponent userProxy houseProxy>
                <HomeOptions />
              </PrivateComponent>
            }
          />

          <Route
            path="/residents"
            element={
              <PrivateComponent userProxy houseProxy>
                <HouseResidents />
              </PrivateComponent>
            }
          />
          <Route
            path="/pantry"
            element={
              <PrivateComponent userProxy houseProxy>
                <Pantry />
              </PrivateComponent>
            }
          />
          <Route
            path="/pantry/:options"
            element={
              <PrivateComponent userProxy houseProxy>
                <PantryOptions />
              </PrivateComponent>
            }
          />
          <Route
            path="/options"
            element={
              <PrivateComponent userProxy houseProxy={false}>
                <Options />
              </PrivateComponent>
            }
          />

          <Route
            path="/user-details"
            element={
              <PrivateComponent userProxy houseProxy>
                <UserDetails />
              </PrivateComponent>
            }
          />
        </Switch>
      </BrowserRouter>
    </Suspense>
  )
}
