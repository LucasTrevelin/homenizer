import React from 'react'
import ResidentOptions from '../components/pages/ResidentOptions'

const ResidentOptionsPage: React.FC = () => {
  return <ResidentOptions />
}

export default ResidentOptionsPage
