import React from 'react'
import OptionsComponent from '../components/pages/Options'

const Options: React.FC = () => {
  return <OptionsComponent />
}

export default Options
