import React from 'react'
import UserDetails from '../components/pages/UserDetails'

const UserDetailsPage: React.FC = () => {
  return <UserDetails />
}

export default UserDetailsPage
