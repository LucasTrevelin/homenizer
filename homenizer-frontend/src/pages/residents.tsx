import React from 'react'
import Residents from '../components/pages/ResidentOptions/Residents'

const HouseResidents: React.FC = () => {
  return <Residents />
}

export default HouseResidents
