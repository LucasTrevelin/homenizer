import React from 'react'
import PantryComponent from '../components/pages/PantryOptions'

const Pantry: React.FC = () => {
  return <PantryComponent />
}

export default Pantry
