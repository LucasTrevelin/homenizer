import React from 'react'
import HomeOptions from '../components/pages/HomeOptions'

const HomeOptionsPage: React.FC = () => {
  return <HomeOptions />
}

export default HomeOptionsPage
