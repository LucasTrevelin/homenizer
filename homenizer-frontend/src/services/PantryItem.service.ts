import {
  IPantryItemRequest,
  IPantryItemResponse,
  IUpdatePantryItemByIdParams
} from '../models/PantryItem.model'
import { IUpdateUserByIdParams, IUserResponse } from '../models/User.model'
import { base } from './base'
import RequestService from './RequestService'

export const createPantryItem = (item: IPantryItemRequest) =>
  RequestService.post<{ message: string; data: IPantryItemResponse }>(
    base().createPantryItem,
    {
      ...item
    }
  )

export const getAllPantryItems = () =>
  RequestService.get<IPantryItemResponse[]>(base().getAllPantryItems)

export const getPantryItemById = (_id: string) =>
  RequestService.get<IPantryItemResponse>(base().pantryItem, {
    params: { _id }
  })

export const getHousePantryItems = (houseId: string) =>
  RequestService.get<IPantryItemResponse[]>(base().getHousePantryItems, {
    params: { houseId }
  })

export const updatePantryItemById = (params: IUpdatePantryItemByIdParams) =>
  RequestService.patch<{ message: string }>(base().pantryItem, { ...params })

export const deletePantryItemById = (_id: string, houseId: string) =>
  RequestService.delete<unknown>(base().pantryItem, {
    data: { _id, houseId }
  })
