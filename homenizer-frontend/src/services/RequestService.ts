import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'

export type RequestConfig = AxiosRequestConfig

export type Response<T> = AxiosResponse<T>

const baseURL = import.meta.env.VITE_BACKEND_BASE_URL

export const axiosInstance = axios.create({
  baseURL: baseURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
})

class RequestService {
  /**
   * Performs a GET request.
   * @param url - Request url
   * @param config  - Request configs
   */
  public static get<T>(
    url: string,
    config: RequestConfig = {}
  ): Promise<Response<T>> {
    return axiosInstance.get<T, Response<T>>(url, config)
  }

  /**
   * Performs a POST request.
   * @param url - Request url
   * @param config  - Request configs
   */
  public static post<T>(
    url: string,
    data?: unknown,
    config?: RequestConfig
  ): Promise<Response<T>> {
    return axiosInstance.post<T, Response<T>>(url, data, config)
  }

  /**
   * Performs a PUT request.
   * @param url - Request url
   * @param config  - Request configs
   */
  public static put<T>(
    url: string,
    data?: unknown,
    config?: RequestConfig
  ): Promise<Response<T>> {
    return axiosInstance.put<T, Response<T>>(url, data, config)
  }

  /**
   * Performs a PATCH request.
   * @param url - Request url
   * @param config  - Request configs
   */
  public static patch<T>(
    url: string,
    data?: unknown,
    config?: RequestConfig
  ): Promise<Response<T>> {
    return axiosInstance.patch<T, Response<T>>(url, data, config)
  }

  /**
   * Performs a DELETE request.
   * @param url - Request url
   * @param config  - Request configs
   */
  public static delete<T>(
    url: string,
    config?: RequestConfig
  ): Promise<Response<T>> {
    return axiosInstance.delete<T, Response<T>>(url, config)
  }

  /**
   * Performs a DELETE request with body.
   * @param url - Request url
   * @param data - Request payload
   * @param config  - Request configs
   */
  public static deleteWithBody<T>(
    url: string,
    data: unknown,
    config?: RequestConfig
  ): Promise<Response<T>> {
    const requestConfig: RequestConfig = {
      method: 'delete',
      url,
      data,
      withCredentials: true,
      ...config
    }

    return axios.request(requestConfig)
  }
}

export default RequestService
