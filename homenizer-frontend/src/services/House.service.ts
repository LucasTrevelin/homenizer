import { IHouse, IHouseRequest, IHouseResponse } from '../models/House.model'
import { base } from './base'
import RequestService from './RequestService'

export const createHouse = (houseRequest: IHouseRequest) =>
  RequestService.post<unknown>(base().createHouse, {
    ...houseRequest
  })

export const deleteNote = (id: string) =>
  RequestService.delete<unknown>(base().house, {
    data: { id }
  })

export const getAllHouses = () =>
  RequestService.get<IHouseResponse[]>(base().getAllHouses)

export const updateNote = (_id: string, changes?: unknown) =>
  RequestService.patch<IHouseResponse>(base().note, {
    _id,
    changes
  })

export const getHouseById = (houseId: string) =>
  RequestService.get<IHouseResponse>(base().house, {
    params: { _id: houseId }
  })
