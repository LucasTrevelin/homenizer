import {
  IJoinHouseParams,
  IRemoveUserFromHouParams,
  IUpdateUserByIdParams,
  IUser,
  IUserResponse,
  IUserResponseLogin
} from '../models/User.model'
import { base } from './base'
import RequestService from './RequestService'

export const createUser = (userCredentials: IUser) =>
  RequestService.post<unknown>(base().createUser, {
    ...userCredentials
  })

export const userSignin = (userCredentials: Partial<IUser>) =>
  RequestService.post<IUserResponseLogin>(base().signin, {
    ...userCredentials
  })

export const getAllUsers = () =>
  RequestService.get<IUserResponse[]>(base().getAllUsers)

export const getUserById = (_id: string) =>
  RequestService.get<IUserResponse>(base().user, { params: { _id } })

export const joinHouse = (params: IJoinHouseParams) =>
  RequestService.patch<{ message: string }>(base().joinHouse, { ...params })

export const updateUserById = (params: IUpdateUserByIdParams) =>
  RequestService.patch<{ message: string }>(base().user, { ...params })

export const getHouseResidents = (houseId: string) =>
  RequestService.get<IUserResponse[]>(base().getHouseUsers, {
    params: { houseId }
  })

export const getHouseAdministrators = (houseId: string, role: string) =>
  RequestService.get<IUserResponse[]>(base().getHouseUsers, {
    params: { houseId, role }
  })

export const removeHouserUser = (params: IRemoveUserFromHouParams) =>
  RequestService.patch<{ message: string }>(base().removeUserFromHouse, {
    ...params
  })
