import {
  IShopList,
  IShopListItem,
  IShopListRequest,
  IShopListResponse,
  IUpdateShopListByIdParams
} from '../models/ShopList.model'
import { base } from './base'
import RequestService from './RequestService'

export const createShopList = (item: IShopListRequest) =>
  RequestService.post<{ message: string; data: IShopListResponse }>(
    base().createShopList,
    {
      ...item
    }
  )

export const getAllShopLists = () =>
  RequestService.get<IShopListResponse[]>(base().getAllShopLists)

export const getShopListById = (_id: string) =>
  RequestService.get<IShopListResponse>(base().shopList, {
    params: { _id }
  })

export const getHouseShopLists = (houseId: string) =>
  RequestService.get<IShopListResponse[]>(base().getHouseShopLists, {
    params: { houseId }
  })

export const updateShopListById = (params: IUpdateShopListByIdParams) =>
  RequestService.patch<{ message: string }>(base().shopList, { ...params })

export const deleteShopListById = (_id: string, houseId: string) =>
  RequestService.delete<{ message: string }>(base().shopList, {
    data: { _id, houseId }
  })
