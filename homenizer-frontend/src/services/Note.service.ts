import { INoteRequest, INoteResponse } from '../models/Note.model'
import { base } from './base'
import RequestService from './RequestService'

export const createNote = (noteRequest: INoteRequest) =>
  RequestService.post<unknown>(base().createNote, {
    ...noteRequest
  })

export const deleteNote = (_id: string, houseId: string) =>
  RequestService.delete<unknown>(base().note, {
    data: { _id, houseId }
  })

export const getAllNotes = (favorite?: boolean) =>
  RequestService.get<INoteResponse[]>(base().getAllNotes, {
    params: { favorite }
  })

export const getHouseNotes = (houseId: string, favorite?: boolean) =>
  RequestService.get<INoteResponse[] | { message: string }>(
    base().getHouseNotes,
    {
      params: { houseId, favorite }
    }
  )

export const updateNote = (_id: string, changes?: unknown) =>
  RequestService.patch<INoteResponse[]>(base().note, {
    _id,
    changes
  })
