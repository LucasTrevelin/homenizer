export const base = () => {
  const baseHost = 'api'

  return {
    //User
    user: `${baseHost}/user`,
    signin: `${baseHost}/auth/signin`,
    createUser: `${baseHost}/user/create`,
    getAllUsers: `${baseHost}/user/all`,
    joinHouse: `${baseHost}/user/join-house`,
    removeUserFromHouse: `${baseHost}/user/remove-from-house`,
    getHouseUsers: `${baseHost}/user/house-users`,

    //Note
    note: `${baseHost}/note`,
    createNote: `${baseHost}/note/create`,
    getAllNotes: `${baseHost}/note/all`,
    getHouseNotes: `${baseHost}/note/house-notes`,

    //House
    house: `${baseHost}/house`,
    createHouse: `${baseHost}/house/create`,
    getAllHouses: `${baseHost}/house/all`,

    //Pantry Item
    pantryItem: `${baseHost}/pantry-item`,
    createPantryItem: `${baseHost}/pantry-item/create`,
    getAllPantryItems: `${baseHost}/pantry-item/all`,
    getHousePantryItems: `${baseHost}/pantry-item/house-pantry-items`,

    //ShopList
    shopList: `${baseHost}/shop-list`,
    createShopList: `${baseHost}/shop-list/create`,
    getAllShopLists: `${baseHost}/shop-list/all`,
    getHouseShopLists: `${baseHost}/shop-list/house-shop-lists`
  }
}
