export enum EURLs {
  Mural = '/home',
  Despensa = '/pantry',
  Itens = '/pantry/items',
  Listas = '/pantry/lists',
  //TODO: to be implemented
  //Contas = '/bills',
  Favoritos = '/home/favorites',
  Escrever = '/home/write',
  Moradores = '/residents',
  Administradores = '/residents/administrators',
  Usuários = '/residents/users',
  Perfil = '/user-details'
}

export enum Elinks {
  Mural = 'Mural',
  Despensa = 'Despensa',
  //TODO: to be implemented
  //Contas = 'Contas',
  Moradores = 'Moradores'
}

export const mapperURL = {
  [Elinks.Mural]: '/home',
  [Elinks.Despensa]: '/pantry',
  [Elinks.Moradores]: '/residents'
  //TODO: to be implemented
  //[Elinks.Contas]: '/bills',
  //[Elinks.Perfil]: '/user-details'
}
