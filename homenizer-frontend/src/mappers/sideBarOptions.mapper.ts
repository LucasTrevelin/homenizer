import { EURLs } from './URL.mapper'

export const sideBarOptions = {
  //Mural
  [EURLs.Mural]: {
    options: ['Ler todos', 'Favoritos', 'Escrever']
  },
  [EURLs.Favoritos]: {
    options: ['Ler todos', 'Favoritos', 'Escrever']
  },
  [EURLs.Escrever]: {
    options: ['Ler todos', 'Favoritos', 'Escrever']
  },
  //Contas
  //[EURLs.Contas]: {
  //  options: ['Mural', 'Despensa', 'Moradores', 'Contas']
  //},
  //Despensa
  [EURLs.Despensa]: {
    options: ['Geral', 'Itens', 'Listas']
  },
  [EURLs.Itens]: {
    options: ['Geral', 'Itens', 'Listas']
  },
  [EURLs.Listas]: {
    options: ['Geral', 'Itens', 'Listas']
  },
  //Moradores
  [EURLs.Moradores]: {
    options: ['Moradores', 'Administradores', 'Usuarios']
  },
  [EURLs.Administradores]: {
    options: ['Moradores', 'Administradores', 'Usuarios']
  },
  [EURLs.Usuários]: {
    options: ['Moradores', 'Administradores', 'Usuarios']
  },
  //Editar perfil
  [EURLs.Perfil]: {
    options: ['Editar perfil']
  }
}

export type TSideBarOptions = ['Ler todos', 'Favoritos', 'Escrever']

export const sideBarOptionsLinkMapper = (option: string) => {
  const initialUrl = import.meta.env.VITE_FRONTEND_BASE_URL
  switch (true) {
    //Board
    case option.replace(' ', '_') === 'Ler_todos':
      return `${initialUrl}/home`
    case option === `Favoritos`:
      return `${initialUrl}/home/favorites`
    case option === `Escrever`:
      return `${initialUrl}/home/write`

    //Despensa
    case option === 'Geral':
      return `${initialUrl}/pantry`
    case option === 'Itens':
      return `${initialUrl}/pantry/items`
    case option === 'Listas':
      return `${initialUrl}/pantry/lists`

    //Moradores
    case option === 'Moradores':
      return `${initialUrl}/residents`
    case option === 'Administradores':
      return `${initialUrl}/residents/administrators`
    case option === 'Usuarios':
      return `${initialUrl}/residents/users`

    //Editar perfil
    case option === `Editar perfil`:
      return `${initialUrl}/user-details`

    default:
      break
  }
}

export const linkSideBarOptionsMapper = (option: string) => {
  switch (option) {
    //Mural
    case '/home':
      return 'Ler todos'

    case '/home/favorites':
      return 'Favoritos'

    case '/home/write':
      return 'Escrever'

    //Moradores
    case '/residents/administrators':
      return 'Administradores'

    case '/residents/users':
      return 'Usuarios'

    case '/user-details':
      return 'Editar perfil'

    //Despensa
    case '/pantry':
      return 'Geral'

    case '/pantry/inventory':
      return 'Itens da despensa'

    case '/pantry/lists':
      return 'Listas'

    default:
      break
  }
}
