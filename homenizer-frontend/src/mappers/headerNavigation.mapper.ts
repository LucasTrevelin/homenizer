import { EURLs } from './URL.mapper'

export const headerNavigation = {
  //Mural
  [EURLs.Mural]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Favoritos]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Escrever]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },

  //Despensa
  [EURLs.Despensa]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Itens]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Listas]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },

  //Moradores
  [EURLs.Moradores]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Administradores]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  [EURLs.Usuários]: {
    options: ['Mural', 'Despensa', 'Moradores']
  },
  //Editar perfil
  [EURLs.Perfil]: {
    options: ['Mural', 'Despensa', 'Moradores']
  }
  //Contas
  //TODO: to be implemented
  //[EURLs.Contas]: {
  //  options: ['Mural', 'Despensa', 'Moradores']
  //}
}
