import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  body {
    /* scrollbar-gutter: stable both-edges; */
    margin: 0;
    padding: 0;
    font-family: 'Lato', sans-serif;;
  }
`

export default GlobalStyle
