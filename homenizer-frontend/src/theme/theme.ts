export const theme = {
  colors: {
    secondary: '#F0762B',
    secondaryVariant: '#EF8547',
    backgroundPrimary: '#F2EEE3',
    black: '#000000',
    blackDisabled: 'rgba(0,0,0,0.63)',
    blackDivider: 'rgba(0,0,0,0.25)',
    tertiary: '#5D5D5D',
    input: '#E5E5E5',
    textWhite: '#FFFFFF',
    textSecondary: '#5065A8',
    error: '#FF0000',
    header: '#C4C4C4',
    textPrimary: '#808EB9',
    backgroundYellow: '#FFF60D',
    backgroundGreen: '#57FF5D',
    backgroundOrange: '#FF7A28',
    backgroundBlue: '#9ADEE9',
    backgroundGray: '#e7e7e7',
    textBlue: '#5297D8',
    danger: '#EB5757',
    warning: '#FFBF34',
    success: '#2BAF3A',
    textDisabled: 'rgba(93, 93, 93, 0.65)'
  }
}
