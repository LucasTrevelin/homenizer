import { capitalize } from 'lodash'

export const selectInitials = (name: string) => {
  const nameArray = name.split(' ')
  const firstInitial = nameArray[0][0]
  const secondInitial = capitalize(nameArray[nameArray.length - 1][0])

  return nameArray.length > 1
    ? `${firstInitial}${secondInitial}`
    : `${firstInitial}`
}
