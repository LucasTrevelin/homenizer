export const auxPantryIcons = (name?: string) => {
  switch (name?.toUpperCase()) {
    case 'PÃO':
      return <img src="/assets/icons/bread-icon.png" alt="bread-icon" />

    case 'OVO':
      return <img src="/assets/icons/egg-icon.png" alt="egg-icon" />

    case 'LEITE':
      return <img src="/assets/icons/milk-icon.png" alt="milk-icon" />

    case 'SAL':
      return <img src="/assets/icons/salt-icon.png" alt="salt-icon" />

    default:
      return (
        <img src="/assets/icons/default-inventory-icon.png" alt="salt-icon" />
      )
  }
}
