import { theme } from '../theme/theme'

export const itemsAvailability = (quantity?: number) => {
  if (!quantity && quantity !== 0)
    return { label: 'vazio', color: theme.colors.textDisabled }
  if (quantity === 0)
    return { label: 'Indisponível', color: theme.colors.danger }
  if (quantity === 1 || (quantity < 1 && quantity > 0))
    return { label: 'Último(a)', color: theme.colors.warning }
  if (quantity > 1) return { label: 'Disponível', color: theme.colors.success }
}
