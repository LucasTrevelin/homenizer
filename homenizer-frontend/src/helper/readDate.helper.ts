const transformToFullDigit = (number: number) => {
  let newNumber
  if (number > 0 && number < 10) {
    return (newNumber = `0${number}`)
  }
  return (newNumber = `${number}`)
}

export const readDate = (date: string) => {
  if (date) {
    const treatedDate = new Date(date)
    let year = Number(treatedDate.getFullYear())
    let month = Number(treatedDate.getMonth())
    let day = Number(treatedDate.getDate())

    const treatMonth = (month: number) => {
      let newMonth = month + 1
      if (month === 13) {
        return (newMonth = 1)
      }
      return newMonth
    }

    return `${transformToFullDigit(day)}/${transformToFullDigit(
      treatMonth(month)
    )}/${transformToFullDigit(year)}`
  }
}
