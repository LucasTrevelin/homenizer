export const maskOnlyNumbers = (arg: string) => arg.replace(/\D/g, '')
