export interface INote {
  message: string
  favorite: boolean
  author: string
  editionAuthor?: string
}

export interface INoteResponse extends INote {
  _id: string
}

export interface INoteRequest extends Omit<INote, 'author'> {
  houseId: string
  author?: string
}
