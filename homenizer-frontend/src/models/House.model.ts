import { INoteResponse } from './Note.model'
import { IPantryItemResponse } from './PantryItem.model'
import { IShopList } from './ShopList.model'
import { IUser, IUserResponse } from './User.model'

export interface IHouse {
  name: string
  users?: IUserResponse[] | string[]
  notes?: INoteResponse[] | string[]
  pantryItems?: IPantryItemResponse[] | string[]
  shopLists?: IShopList[] | string[]
}

export interface IHouseResponse extends IHouse {
  _id: string
}

export interface IHouseRequest extends IHouse {
  adminUser: IUser
}
