import { IHouseResponse } from './House.model'

export interface IUser {
  _id: string
  username: string
  email: string
  password: string
}

export interface IUserResponse {
  _id: string
  username: string
  email: string
  password: string
  roles: IRoles[]
  accessToken: string
  house?: string
}
export interface IUserRequest extends IUser {
  roles: string
  accessToken: string
  house?: string
}

export interface IAuthUser extends IUser {
  roles: string[]
  accessToken: string
  house?: string
}

export interface IUserResponseLogin {
  user: IAuthUser
  house?: IHouseResponse
}

export interface IRoles {
  _id: string
  name: string
}

export interface IJoinHouseParams {
  userId: string
  houseId: string
}

export interface IRemoveUserFromHouParams extends IJoinHouseParams {}

export interface IUpdateUserByIdParams {
  _id: string
  changes: Partial<IUserRequest>
}
