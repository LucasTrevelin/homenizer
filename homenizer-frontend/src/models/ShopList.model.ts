export interface IShopList {
  name: string
  lastUpdate: string
  shopListItems: IShopListItem[]
}

export interface IShopListCreate extends Omit<IShopList, 'lastUpdate'> {
  _id?: string
}

export interface IShopListItem {
  name: string
  checked: boolean
  quantity: number
}

export interface IShopListRequest extends IShopListCreate {
  houseId: string
}

export interface IShopListItemResponse extends IShopListItem {
  _id?: number
}

export interface IShopListItemCreate extends IShopListItem {
  id?: number
}

export interface IShopListResponse extends Omit<IShopList, 'shopListItem'> {
  _id: string
  shopListItems: IShopListItemResponse[]
}

export interface IUpdateShopListByIdParams {
  _id: string
  changes: Partial<IShopList>
}
