export interface IPantryItem {
  name: string
  quantity: number
}

export interface IPantryItemResponse extends IPantryItem {
  _id: string
}

export interface IPantryItemRequest extends IPantryItem {
  houseId: string
}

export interface IUpdatePantryItemByIdParams {
  _id: string
  changes: Partial<IPantryItem>
}
