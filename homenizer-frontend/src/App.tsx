import React from 'react'
import 'react-toastify/dist/ReactToastify.css'
import { Routes } from './Routes'
import { ThemeProvider } from 'styled-components'
import { theme } from './theme/theme'
import GlobalStyle from './theme/globalStyle'
import { ToastContainer } from 'react-toastify'
import { UserProvider } from './context/User.context'
import 'react-tooltip/dist/react-tooltip.css'

function App() {
  return (
    <UserProvider>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <ToastContainer />
        <Routes />
      </ThemeProvider>
    </UserProvider>
  )
}

export default App
