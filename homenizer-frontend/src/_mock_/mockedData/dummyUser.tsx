export const dummyUserCredentials = {
  user: {
    _id: '64127090493d3ae8791008f3',
    username: 'Dummy User',
    email: 'dummy@gmail.com',
    roles: ['ROLE_USER'],
    accessToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NDEyNzA5MDQ5M2QzYWU4NzkxMDA4ZjMiLCJpYXQiOjE2Nzg5MzAwNzcsImV4cCI6MTY3OTAxNjQ3N30.t0HOGZXG3DQ9s6zCVpppNE2UPkjapQ2FQFV8gj3Ghsk'
  }
}
