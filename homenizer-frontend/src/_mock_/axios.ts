import axios from 'axios'
import mockAdapter from 'axios-mock-adapter'

const mockedAxios = new mockAdapter(axios)

export const mockedGet = (payload: unknown) => {
  return mockedAxios.onGet('api/note/house-notes').reply(200, {
    data: payload
  })
}
