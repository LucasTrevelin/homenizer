import React from 'react'
import styled from 'styled-components'
import { theme } from '../../theme/theme'

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  width?: string
  height?: string
  placeholder?: string
  borderRadius?: string
  margin?: string
  fontSize?: string
  variant: 'filled' | 'unfilled' | 'danger' | 'success'
}

const backgroundColorVariant = (
  variant: 'filled' | 'unfilled' | 'danger' | 'success'
) => {
  if (variant === 'filled') {
    return `${theme.colors.secondary}`
  }
  if (variant === 'unfilled') {
    return `transparent`
  }
  if (variant === 'danger') {
    return `${theme.colors.danger}`
  }
  if (variant === 'success') {
    return `${theme.colors.success}`
  }
}

const colorVariant = (
  variant: 'filled' | 'unfilled' | 'danger' | 'success'
) => {
  if (variant === 'filled') {
    return `${theme.colors.textWhite}`
  }
  if (variant === 'unfilled') {
    return `${theme.colors.black}`
  }
  if (variant === 'danger') {
    return `${theme.colors.textWhite}`
  }
  if (variant === 'success') {
    return `${theme.colors.textWhite}`
  }
}

export const StyledButton = styled.button<ButtonProps>`
  background-color: ${({ variant }) => backgroundColorVariant(variant)};
  width: ${({ width }) => width || '245px'};
  height: ${({ height }) => height || '60px'};
  border-radius: ${({ borderRadius }) => borderRadius || '8px'};
  border: transparent;
  margin: ${({ margin }) => margin && margin};
  font-size: ${({ fontSize }) => fontSize || 18};
  font-weight: bold;
  color: ${({ variant }) => colorVariant(variant)};
  border: ${({ variant, theme }) =>
    variant === 'unfilled' && `1px solid ${theme.colors.black}`};
  transition: filter ease-in-out 0.2s;
  filter: ${({ disabled }) => disabled && 'brightness(0.6)'};
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'select')}!important;

  :hover {
    cursor: pointer;
    filter: brightness(0.8);
  }
`
