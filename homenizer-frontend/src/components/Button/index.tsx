import React from 'react'
import { StyledButton } from './styles'

interface InputProps extends React.HTMLProps<HTMLButtonElement> {
  width?: string
  height?: string
  placeholder?: string
  borderRadius?: string
  margin?: string
  fontSize?: string
  variant: 'filled' | 'unfilled' | 'danger' | 'success'
  title: string
}

export const Button: React.FC<InputProps> = ({
  width,
  height,
  placeholder,
  borderRadius,
  margin,
  fontSize,
  variant,
  title,
  onClick,
  disabled
}) => {
  return (
    <StyledButton
      disabled={disabled}
      placeholder={placeholder}
      width={width}
      height={height}
      borderRadius={borderRadius}
      margin={margin}
      fontSize={fontSize}
      variant={variant}
      onClick={onClick}
    >
      {title}
    </StyledButton>
  )
}
