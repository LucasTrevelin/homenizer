import React from 'react'
import { Required, StyledInput } from './styles'

interface InputProps {
  width?: string
  height?: string
  placeholder?: string
  borderRadius?: string
  margin?: string
  state?: string
  setState: (state: string) => void
  required?: boolean
  requiredMessage?: string
  name: string
  type?: string
  disabled?: boolean
  padding?: string
  fontSize?: string
}

export const Input: React.FC<InputProps> = ({
  name,
  width,
  height,
  placeholder,
  borderRadius,
  margin,
  state,
  setState,
  required,
  requiredMessage,
  type,
  disabled,
  padding,
  fontSize
}) => {
  return (
    <>
      <StyledInput
        name={name}
        placeholder={placeholder}
        value={state}
        onChange={(event) => setState(event.target.value)}
        type={type}
        width={width}
        height={height}
        borderRadius={borderRadius}
        margin={margin}
        disabled={disabled}
        padding={padding}
        fontSize={fontSize}
      />
      {required && (
        <Required>{requiredMessage || `* ${name} is required.`}</Required>
      )}
    </>
  )
}
