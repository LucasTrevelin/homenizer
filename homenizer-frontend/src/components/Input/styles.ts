import styled from 'styled-components'

interface InputProps {
  width?: string
  height?: string
  placeholder?: string
  borderRadius?: string
  margin?: string
  padding?: string
  fontSize?: string
}

export const Container = styled.div`
  display: flex;
`

export const StyledInput = styled.input<InputProps>`
  background-color: ${({ theme, disabled, type }) =>
    disabled
      ? 'rgba(16,16,16,0.3) !important'
      : type === 'unfilled' || type === 'square'
      ? 'transparent'
      : theme.colors.input};
  width: ${({ width }) => width || '352px'};
  height: ${({ height }) => height || '50px'};
  border-radius: ${({ borderRadius }) => borderRadius || '8px'};
  border: ${({ type }) => !type && 'transparent'};
  border-right: ${({ type, theme }) =>
    type === 'unfilled'
      ? 'transparent'
      : type === 'square'
      ? `1px solid ${theme.colors.black}`
      : 'transparent'};
  border-left: ${({ type, theme }) =>
    type === 'unfilled'
      ? 'transparent'
      : type === 'square'
      ? `1px solid ${theme.colors.black}`
      : 'transparent'};
  border-top: ${({ type, theme }) =>
    type === 'unfilled'
      ? 'transparent'
      : type === 'square'
      ? `1px solid ${theme.colors.black}`
      : 'transparent'};
  border-bottom: ${({ theme, type }) =>
    type && `1px solid ${theme.colors.black}`};
  margin: ${({ margin }) => margin && margin};
  padding: ${({ padding }) => (padding ? padding : '0 1rem')};
  font-size: ${({ fontSize }) => (fontSize ? fontSize : '13')};
  font-weight: bold;
  color: ${({ theme }) => theme.colors.black};
  cursor: ${({ disabled }) => disabled && 'not-allowed'};
  outline: ${({ type }) => type && 'none'};
  height: ${({ type }) => type === 'unfilled' && '20px'};
  text-align: ${({ type }) => type === 'square' && 'center'};

  ::placeholder {
    font-size: ${({ fontSize }) => (fontSize ? fontSize : '13')};
    font-weight: bold;
    color: ${({ theme }) => theme.colors.tertiary};
  }
`

export const Required = styled.div`
  width: 100%;
  text-align: left;
  color: ${({ theme }) => theme.colors.error};
  font-size: 13px;
`
