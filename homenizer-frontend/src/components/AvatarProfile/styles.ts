import styled from 'styled-components'
import { AvatarProfileProps } from '.'

export const ContainerButton = styled.button`
  border-radius: 50%;
  height: 100%;
  width: 100%;
  border: none;
  background-color: transparent;
  z-index: 2;
  position: absolute;
  cursor: pointer;
  outline: 0;
`

export const Container = styled.div<AvatarProfileProps>`
  border-radius: 50%;
  position: relative;
  height: ${({ type }) => {
    if (type === 'large') return '5.2rem'
    if (type === 'medium') return '4.4rem'
    if (type === 'small') return '2.2rem'
  }};
  aspect-ratio: 1/1;
  border: ${({ theme }) => `1px solid ${theme.colors.textPrimary}`};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${({ type }) => {
    if (type === 'large') return '1.87rem'
    if (type === 'medium') return '1.37rem'
    if (type === 'small') return '0.87rem'
  }};
  background-color: ${({ theme }) => theme.colors.backgroundPrimary};
  color: ${({ theme }) => theme.colors.textPrimary};
`
