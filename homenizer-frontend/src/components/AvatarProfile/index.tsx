import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { selectInitials } from '../../helper/selectInitials.helper'
import { Dropdown } from '../DropDown'

import * as S from './styles'

export interface AvatarProfileProps {
  userName?: string
  type: 'small' | 'medium' | 'large'
  isClickable?: boolean
}

const AvatarProfile: React.FC<AvatarProfileProps> = ({
  userName,
  type,
  isClickable
}) => {
  const [isOpen, setIsOpen] = useState(false)

  const navigate = useNavigate()

  const logout = () => {
    localStorage.removeItem('homenizer-app')
    localStorage.removeItem('user-house')
    navigate('/')
  }

  const profileEdit = () => {
    navigate('/user-details')
  }

  return (
    <S.Container type={type}>
      {isClickable ? (
        <S.ContainerButton onClick={() => setIsOpen(!isOpen)} />
      ) : (
        <></>
      )}
      <Dropdown
        isOpen={isOpen}
        options={[
          { label: 'Logout', onClick: logout },
          { label: 'Editar perfil', onClick: profileEdit }
        ]}
        username={userName || 'User Unknown'}
      />

      {selectInitials(userName || 'User Unknown')}
    </S.Container>
  )
}

export default AvatarProfile
