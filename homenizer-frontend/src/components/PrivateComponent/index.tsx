import React, { useMemo } from 'react'
import { Navigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { IHouseResponse } from '../../models/House.model'
import { IUserResponse } from '../../models/User.model'

interface Props {
  children: React.ReactNode
  userProxy: boolean
  houseProxy: boolean
}

const PrivateComponent: React.FC<Props> = ({
  children,
  houseProxy,
  userProxy
}) => {
  const savedHouse: IHouseResponse = JSON.parse(
    localStorage.getItem('user-house')!
  )

  const savedUser: IUserResponse = JSON.parse(
    localStorage.getItem('homenizer-app')!
  )

  const isLoggedIn = !!savedUser

  const notResident = !savedHouse

  const errorMessage = (message: string, toastId: string) => {
    toast.error(message, {
      toastId
    })
  }

  if (userProxy && !isLoggedIn)
    return (
      <>
        {errorMessage(
          'Área restrita para usuários autenticados, por favor faça o login.',
          'not-logged-user'
        )}

        <Navigate to="/login" />
      </>
    )
  if (houseProxy && isLoggedIn && notResident) {
    return (
      <>
        {errorMessage(
          'Você ainda não é residente, por favor escolha uma casa existente ou crie uma nova.',
          'not-resident-error'
        )}
        <Navigate to="/options" />
      </>
    )
  }
  return <>{children}</>
}

export default PrivateComponent
