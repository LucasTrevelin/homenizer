import React from 'react'
import * as S from './styles'

export interface IBadge {
  label: string
  color?: string
}

const Badge: React.FC<IBadge> = ({ label, color }) => {
  return <S.Container color={color}>{label}</S.Container>
}

export default Badge
