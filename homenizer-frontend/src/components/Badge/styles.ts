import styled from 'styled-components'
import { IBadge } from '.'
interface IContainer extends Pick<IBadge, 'color'> {}

export const Container = styled.p<IContainer>`
  font-size: 12px;
  color: ${({ theme, color }) => (color ? `${color}` : theme.colors.secondary)};
  margin-bottom: 0;
  padding: 5px 10px;
  border: ${({ theme, color }) =>
    color ? `1px solid ${color}` : `1px solid ${theme.colors.secondary}`};
  border-radius: 10px;
`
