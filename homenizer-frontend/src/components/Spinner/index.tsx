import React from 'react'
import { Oval } from 'react-loader-spinner'
import { theme } from '../../theme/theme'
import { Container } from './styles'

export interface SpinnerProps {
  isLoading: boolean
}

const Spinner: React.FC<SpinnerProps> = ({ isLoading }) => {
  return isLoading ? (
    <Container>
      <Oval
        height="100"
        width="100"
        color={theme.colors.secondary}
        secondaryColor={theme.colors.secondaryVariant}
        ariaLabel="loading"
      />
    </Container>
  ) : (
    <></>
  )
}

export default Spinner
