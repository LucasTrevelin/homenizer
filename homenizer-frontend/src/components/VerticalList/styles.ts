import styled from 'styled-components'

export const ListSection = styled.section`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  height: 100%;
  row-gap: 0.5rem;
  padding-top: 1.25rem;
`

export const ListTitle = styled.div`
  font-size: 20px;
  color: ${({ theme }) => theme.colors.tertiary};
  font-weight: bold;
`

export const ListContainerVertical = styled.div<{ itemsLength: number }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  row-gap: 1rem;
  overflow-y: auto;
  max-height: 50vh;
  width: 100%;
  padding: 6px 0;
  /* TODO: logic to represent overflow by shadow */
  /*box-shadow: 0px 8px 3px -3px rgba(0, 0, 0, 0.75);*/
  z-index: 10;
`
export const EmptyBoard = styled.div`
  width: 100%;
  height: 100%;
  margin-top: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
`
