import React, { ReactNode } from 'react'
import { IPantryItemResponse } from '../../models/PantryItem.model'
import { IShopListResponse } from '../../models/ShopList.model'
import { ListComponent } from '../ListComponent'

import * as S from './styles'

export interface IVerticalList {
  listTitle: string
  itemList: (IPantryItemResponse | IShopListResponse)[]
  emptyMessage?: string
  listType: 'shopList' | 'pantryItem'
  listFooter?: ReactNode
  refetch?: () => Promise<void>
}

const VerticalList: React.FC<IVerticalList> = ({
  itemList,
  listTitle,
  emptyMessage,
  listType,
  listFooter,
  refetch
}) => {
  return (
    <S.ListSection>
      <S.ListTitle>{listTitle}</S.ListTitle>
      <S.ListContainerVertical itemsLength={itemList?.length}>
        {itemList?.length ? (
          itemList?.map((item) => (
            <ListComponent
              key={`post-it-${`item-${item._id}`}`}
              item={item}
              type={listType}
              refetch={refetch}
            />
          ))
        ) : emptyMessage ? (
          <S.EmptyBoard>{emptyMessage}</S.EmptyBoard>
        ) : (
          <></>
        )}
      </S.ListContainerVertical>
      {listFooter}
    </S.ListSection>
  )
}

export default VerticalList
