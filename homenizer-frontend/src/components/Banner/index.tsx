import React from 'react'
import * as S from './styled'

export interface IBanner {
  assetPath: string
}

const Banner: React.FC<IBanner> = ({ assetPath }) => {
  return <S.BannerFrame assetPath={assetPath} />
}

export default Banner
