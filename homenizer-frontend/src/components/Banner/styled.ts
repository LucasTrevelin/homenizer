import styled from 'styled-components'
import { IBanner } from '.'

export const BannerFrame = styled.div<IBanner>`
  width: 100%;
  height: 10rem;
  background-image: ${({ assetPath }) =>
    `url(/assets/images/${assetPath}.png)`};
  opacity: 0.8;
  background-repeat: space;
  background-position: center;
  background-color: ${({ theme }) => theme.colors.backgroundPrimary};
`
