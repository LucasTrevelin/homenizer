import styled from 'styled-components'
import { ICard } from '.'

interface ICardContainer extends Pick<ICard, 'type'> {}

export const Container = styled.div<ICardContainer>`
  aspect-ratio: 1/1;
  width: 220px;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 10px;
  box-shadow: 0 4px 4px 1px rgb(0 0 0 / 0.25);
  display: flex;
  flex-direction: column;
  row-gap: ${({ type }) => type === 'pantryItem' && ' 0.25rem'};
  justify-content: center;
  align-items: center;
  & > button {
    border-radius: 50%;
    background-color: transparent;
    border: transparent;
    padding: 0;
    cursor: pointer;
  }
`

export const StyledLink = styled.a`
  font-size: 18px;
  text-decoration: none;
  cursor: pointer;
  color: ${({ theme }) => theme.colors.textBlue};
  margin-top: 12px;
  :hover {
    text-decoration: underline;
  }
`
export const EmptyBadge = styled.div`
  height: 38.5px;
`

export const StyledName = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-top: 12px;
  margin: 0 1rem;
  text-align: center;
  margin-top: 0.5rem;
`
export const Button = styled.button`
  aspect-ratio: 1/1;
  min-width: 70px;
  padding: 1;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 50%;
  border: transparent;
  outline: none;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 4;
  transition: all 0.2s ease-in-out;

  :hover {
    transition: all 0.2s ease-in-out;
    cursor: pointer;
    filter: brightness(0.9);
  }
`

export const BadgeContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  row-gap: 1rem;
  min-width: 86px;
`
export const EmptyIcon = styled.div`
  aspect-ratio: 1/1;
  height: 38px;
`
export const ItemQuantity = styled.p`
  font-size: 18px;
  margin: 0 1rem;
`

export const CardFooter = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 1.5rem;
`

export const UpdateQuantityActionContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
`

export const ActionsPantryItemContainer = styled.div`
  display: flex;
  align-items: center;
  column-gap: 0.5rem;
  margin-right: 1rem;
`

export const ActionsPantryButton = styled.button`
  aspect-ratio: 1/1;
  min-width: 20px;
  padding: 1;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 50%;
  border: ${({ theme }) => `2px solid ${theme.colors.black}`};
  outline: none;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 4;
  transition: all 0.2s ease-in-out;

  :hover {
    transition: all 0.2s ease-in-out;
    cursor: pointer;
    filter: brightness(0.9);
  }
`
