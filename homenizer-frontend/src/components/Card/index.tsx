import { AxiosError } from 'axios'
import React, { memo, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { auxPantryIcons } from '../../helper/auxPantryIcons.helper'
import { itemsAvailability } from '../../helper/itemsAvailability.helper'
import { unityDescriptionHasPlural } from '../../helper/unityDescriptionHasPlural.helper'
import { IPantryItemResponse } from '../../models/PantryItem.model'
import { IRoles, IUserResponse } from '../../models/User.model'
import { updatePantryItemById } from '../../services/PantryItem.service'
import AvatarProfile from '../AvatarProfile'
import Badge from '../Badge'
import * as S from './styles'
import 'react-tooltip/dist/react-tooltip.css'

export interface ICard {
  type?: 'addPantryItem' | 'pantryItem'
  user?: IUserResponse
  pantryItem?: IPantryItemResponse
  onClick?: () => void | ((modalState: boolean) => void)
  setIsLoading?: (isLoading: boolean) => void
  refetch?: () => void
}

const Card: React.FC<ICard> = ({
  type,
  user,
  onClick,
  pantryItem,
  setIsLoading,
  refetch
}) => {
  const isAdmin = (roles?: IRoles[]) => {
    let hasAdminRole: number = 0

    if (!roles) return false
    roles.some((role) => {
      return role.name === 'admin' ? (hasAdminRole += 1) : 0
    })

    return !!hasAdminRole
  }

  const pantryItemAvailability = itemsAvailability(pantryItem?.quantity)

  const auxIcon = auxPantryIcons(pantryItem?.name)

  const handleAddQuantityProductById = async () => {
    if (setIsLoading) setIsLoading(true)

    if (pantryItem) {
      try {
        const response = await updatePantryItemById({
          _id: pantryItem._id,
          changes: { quantity: pantryItem.quantity + 1 }
        })
        toast.success(response.data.message, {
          toastId: 'add-quantity-item-success1'
        })
        if (setIsLoading) setIsLoading(false)
        refetch && refetch()
      } catch (error) {
        if (setIsLoading) setIsLoading(false)
        toast.error(
          (error as AxiosError).message === 'Network Error'
            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
            : (
                (error as AxiosError).response?.data as {
                  message: string
                }
              ).message,
          { toastId: 'add-quantity-item-error' }
        )
      }
    }
  }

  const handleRemoveQuantityProductById = async () => {
    if (setIsLoading) setIsLoading(true)
    if (pantryItem) {
      try {
        const response = await updatePantryItemById({
          _id: pantryItem._id,
          changes: { quantity: pantryItem.quantity - 1 }
        })
        toast.success(response.data.message, {
          toastId: 'remove-quantity-item-error'
        })
        refetch && refetch()
      } catch (error) {
        if (setIsLoading) setIsLoading(false)
        return toast.error(
          (error as AxiosError).message === 'Network Error'
            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
            : (
                (error as AxiosError).response?.data as {
                  message: string
                }
              ).message,
          { toastId: 'remove-quantity-item-error' }
        )
      }
    }
  }

  if (type === 'addPantryItem') {
    return (
      <S.Container>
        <S.Button onClick={onClick}>
          <img alt="add-card" src="/assets/icons/add-icon.png" />
        </S.Button>
      </S.Container>
    )
  }
  if (type === 'pantryItem') {
    return (
      <S.Container type={type}>
        <S.StyledName>{pantryItem?.name}</S.StyledName>
        {auxIcon ? auxIcon : <S.EmptyIcon />}
        <S.BadgeContainer>
          <Badge
            label={pantryItemAvailability!.label}
            color={pantryItemAvailability!.color}
          />
        </S.BadgeContainer>
        <S.CardFooter>
          <S.UpdateQuantityActionContainer>
            <S.ItemQuantity>
              {unityDescriptionHasPlural(pantryItem?.quantity)
                ? `${!pantryItem?.quantity ? 0 : pantryItem?.quantity} unidades`
                : `${pantryItem?.quantity} unidade`}
            </S.ItemQuantity>
            <S.ActionsPantryItemContainer>
              <S.ActionsPantryButton onClick={handleAddQuantityProductById}>
                <img src="/assets/icons/plus-icon.png" />
              </S.ActionsPantryButton>
              {pantryItem?.quantity !== 0 ? (
                <S.ActionsPantryButton
                  onClick={handleRemoveQuantityProductById}
                >
                  <img src="/assets/icons/negative-icon.png" />
                </S.ActionsPantryButton>
              ) : (
                <></>
              )}
            </S.ActionsPantryItemContainer>
          </S.UpdateQuantityActionContainer>
        </S.CardFooter>
      </S.Container>
    )
  }
  return (
    <S.Container type={type}>
      <AvatarProfile type="medium" userName={user?.username} />
      <S.StyledName>{user?.username}</S.StyledName>
      {isAdmin(user?.roles) ? <Badge label="Admin" /> : <S.EmptyBadge />}
      <S.StyledLink onClick={onClick}>Ver detalhes</S.StyledLink>
    </S.Container>
  )
}

export default memo(Card)
