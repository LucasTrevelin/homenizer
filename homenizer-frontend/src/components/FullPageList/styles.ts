import styled from 'styled-components'

export const ListSection = styled.section<{
  itemsLength: number
  columnWidth: string
  rowWidth: string
  maxItemsFlex: number
  flexDirection?: 'column'
  maxHeight?: string
  footer?: boolean
}>`
  display: ${({ itemsLength, maxItemsFlex }) =>
    itemsLength <= maxItemsFlex ? 'flex' : 'grid'};
  grid-gap: 1rem;
  padding: 3rem 1rem 1rem 1rem;
  row-gap: 1rem;
  flex-direction: ${({ flexDirection }) => flexDirection};
  grid-template-columns: ${({ itemsLength, columnWidth }) =>
    itemsLength > 3 && `repeat(auto-fill, minmax(${columnWidth}, 1fr))`};
  grid-template-rows: ${({ itemsLength, maxItemsFlex }) =>
    itemsLength > maxItemsFlex && '1fr 1fr'};
  justify-content: center;
  align-items: center;
  grid-auto-flow: ${({ itemsLength, maxItemsFlex }) =>
    itemsLength > maxItemsFlex && 'row'};
  grid-auto-rows: ${({ itemsLength, maxItemsFlex, rowWidth }) =>
    itemsLength > maxItemsFlex + 1 && `minmax(${rowWidth}, 1fr)`};
  overflow-y: auto;
  max-width: 85.3vw;
  box-shadow: ${({ footer }) => !footer && '10px 0 10px 0px rgba(0 0 0 / 0.5)'};
  min-height: ${({ maxHeight }) => (maxHeight && maxHeight) || '55.5vh'};
  max-height: ${({ maxHeight }) => (maxHeight && maxHeight) || '55.5vh'};
  width: 100%;
  justify-items: center;
`
export const ContainerWithFooter = styled.div<{ footer?: boolean }>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  box-shadow: ${({ footer }) => footer && '10px 0 10px 0px rgba(0 0 0 / 0.5)'};
  padding-bottom: 1rem;
  width: 100%;
`
