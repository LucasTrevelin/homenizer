import React, { ReactNode } from 'react'

import * as S from './styles'

interface IFullPageList {
  columnWidth: string
  rowWidth: string
  itemsLength: number
  children: ReactNode
  maxItemsFlex: number
  flexDirection?: 'column'
  maxHeight?: string
  footer?: ReactNode
}

const FullPageList: React.FC<IFullPageList> = ({
  children,
  columnWidth,
  rowWidth,
  itemsLength,
  maxItemsFlex,
  flexDirection,
  maxHeight,
  footer
}) => {
  return (
    <S.ContainerWithFooter footer={!!footer}>
      <S.ListSection
        itemsLength={itemsLength}
        columnWidth={columnWidth}
        rowWidth={rowWidth}
        maxItemsFlex={maxItemsFlex}
        flexDirection={flexDirection}
        maxHeight={maxHeight}
        footer={!!footer}
      >
        {children}
      </S.ListSection>
      {footer}
    </S.ContainerWithFooter>
  )
}

export default FullPageList
