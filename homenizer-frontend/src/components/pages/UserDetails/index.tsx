import { AxiosError } from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { UserContext } from '../../../context/User.context'
import { IHouseResponse } from '../../../models/House.model'
import { IAuthUser } from '../../../models/User.model'
import { getUserById, updateUserById } from '../../../services/User.service'
import AvatarProfile from '../../AvatarProfile'
import { Button } from '../../Button'
import { Input } from '../../Input'
import MainPage from '../../MainPage'

import * as S from './styles'

interface IUserDetails {
  refetchInformations?: () => void
}

const UserDetails: React.FC<IUserDetails> = ({ refetchInformations }) => {
  const savedUser = JSON.parse(
    localStorage.getItem('homenizer-app')!
  ) as IAuthUser

  const [username, setUsername] = useState(savedUser.username)
  const [email, setEmail] = useState(savedUser.email)

  const userContext = useContext(UserContext)
  const { saveEditInfo } = userContext

  const isSaveDisabled = () => {
    return (
      (savedUser.username === username && savedUser.email === email) ||
      !username ||
      !email
    )
  }

  const userHasAdminRole = (user: IAuthUser) => {
    let hasAdminRole: number = 0
    if (!user.roles) return false
    user.roles.some((role) => {
      return role === 'ROLE_ADMIN' ? (hasAdminRole += 1) : 0
    })
    return !!hasAdminRole
  }

  const savedUserRefresh = async () => {
    try {
      const response = await getUserById(savedUser._id)
      await saveEditInfo(response.data)
    } catch (error) {
      return toast.error(
        (error as AxiosError).message === 'Network Error'
          ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
          : (
              (error as AxiosError).response?.data as {
                message: string
              }
            ).message
      )
    }
  }

  const handleProfiletEdition = async () => {
    try {
      const response = await updateUserById({
        _id: savedUser._id,
        changes: {
          username,
          email
        }
      })
      await savedUserRefresh()
      toast.success(response.data.message)
    } catch (error) {
      return toast.error(
        (error as AxiosError).message === 'Network Error'
          ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
          : (
              (error as AxiosError).response?.data as {
                message: string
              }
            ).message
      )
    }
  }

  return (
    <MainPage
      assetPath=""
      subTitle="Administre as informações do seu perfil"
      title="Edite seu perfil"
      headerHasNavigation
    >
      <S.FormContainer>
        <S.BodyContainer>
          <S.NameContainer>
            <AvatarProfile type="large" userName={username} />
            <S.InputContainer>
              <label>Nome completo</label>
              <Input
                placeholder="Nome"
                state={username}
                setState={setUsername}
                name="name"
                type="text"
                width="500px!important"
              />
            </S.InputContainer>
          </S.NameContainer>
          <S.InputContainer>
            <label>E-mail</label>
            <Input
              placeholder="E-mail"
              state={email}
              setState={setEmail}
              name="email"
              type="email"
              width="610px"
            />
          </S.InputContainer>
          <S.AdminContainer>
            <label>Tem acesso como administrador?</label>
            <S.CheckboxesContainer>
              <S.OptionContainer>
                <S.InputCheckbox disabled={true}>
                  <S.InputCheckMark isSelected={!userHasAdminRole(savedUser)} />
                </S.InputCheckbox>
                <p>Não</p>
              </S.OptionContainer>
              <S.OptionContainer>
                <S.InputCheckbox disabled={true}>
                  <S.InputCheckMark isSelected={userHasAdminRole(savedUser)} />
                </S.InputCheckbox>
                <p>Sim</p>
              </S.OptionContainer>
            </S.CheckboxesContainer>
          </S.AdminContainer>
        </S.BodyContainer>

        <S.ButtonsContainer>
          <Button
            variant="filled"
            title="Salvar"
            width="150px"
            onClick={() => handleProfiletEdition()}
            disabled={isSaveDisabled()}
          />
        </S.ButtonsContainer>
      </S.FormContainer>
    </MainPage>
  )
}

export default UserDetails
