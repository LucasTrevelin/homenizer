import React from 'react'
import { useLocation } from 'react-router-dom'
import PantryGeneral from './PantryGeneral'
import PantryItems from './PantryItems'
import PantryLists from './PantryLists'

const PantryOptions: React.FC = () => {
  const location = useLocation()

  switch (location.pathname) {
    case '/pantry':
      return <PantryGeneral />
    case '/pantry/items':
      return <PantryItems />
    default:
      return <PantryLists />
  }
}

export default PantryOptions
