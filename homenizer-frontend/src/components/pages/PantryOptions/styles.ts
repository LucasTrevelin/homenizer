import styled from 'styled-components'

export const Container = styled.div`
  height: 100vh;
  max-width: 100vw;
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Content = styled.div`
  display: flex;
  height: 100%;
`

export const PageContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

export const TitleSection = styled.div`
  margin-top: 2rem;
  width: 100%;
  text-align: center;
  & > p {
    font-size: 1.5rem;
    font-weight: 400;
  }
`
export const PostItContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: 1fr 1fr;
  row-gap: 1rem;
  column-gap: 2rem;
  margin: 2rem auto;
`

export const PostIt = styled.div`
  height: 8.5rem;
  aspect-ratio: 3/1;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  background-color: yellow;
  border-radius: 10px;
`
