import { AxiosError } from 'axios'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { toast } from 'react-toastify'
import { IPantryItemResponse } from '../../../../models/PantryItem.model'
import { IShopListResponse } from '../../../../models/ShopList.model'
import { getHousePantryItems } from '../../../../services/PantryItem.service'
import { getHouseShopLists } from '../../../../services/ShopList.service'
import MainPage from '../../../MainPage'
import Spinner from '../../../Spinner'
import VerticalList from '../../../VerticalList'
import * as S from './styles'

const PantryGeneral: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [pantryItems, setPantryItems] = useState<IPantryItemResponse[]>([])
  const [shopLists, setShopLists] = useState<IShopListResponse[]>([])

  const pantryItemListFooter = () => {
    return pantryItems.length ? (
      <S.ListFooter href={'/pantry/items'}>
        Navegar para área de itens da despensa
      </S.ListFooter>
    ) : (
      <S.ListFooter href={'/pantry/items'}>
        Clique aqui para criar um item!
      </S.ListFooter>
    )
  }

  const shopListFooter = () => {
    return shopLists.length ? (
      <S.ListFooter href={'/pantry/lists'}>
        Navegar para área de listas de compras
      </S.ListFooter>
    ) : (
      <S.ListFooter href={'/pantry/lists'}>
        Clique aqui para criar uma lista!
      </S.ListFooter>
    )
  }

  const fetchHouseAllPantry = useCallback(async () => {
    const savedHouse = await JSON.parse(localStorage.getItem('user-house')!)
    setIsLoading(true)
    if (savedHouse._id) {
      await getHousePantryItems(savedHouse._id)
        .then((response) => {
          if (response.status === 200 && !response.data.length) {
            toast.warning(
              'Ainda não existem itens criados na despensa desta casa.',
              { toastId: 'fetch-house-pantry-items-warning-1' }
            )
          }
          setPantryItems(response.data)
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          return toast.error(
            (error as AxiosError).message === 'Network Error'
              ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
              : (
                  (error as AxiosError).response?.data as {
                    message: string
                  }
                ).message
          )
        })
      await getHouseShopLists(savedHouse._id)
        .then((response) => {
          if (response.status === 200 && !response.data.length) {
            toast.warning('Ainda não existem listas criadas nesta casa.', {
              toastId: 'fetch-house-shop-lists-warning-1'
            })
          }
          setShopLists(response.data)
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          return toast.error(
            (error as AxiosError).message === 'Network Error'
              ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
              : (
                  (error as AxiosError).response?.data as {
                    message: string
                  }
                ).message
          )
        })
    }

    setIsLoading(false)
  }, [])

  useEffect(() => {
    fetchHouseAllPantry()
  }, [])

  return (
    <MainPage
      title="Geral"
      subTitle="Visão geral dos itens da despensa e das listas de compras"
      assetPath={'pantry_banner'}
      headerHasNavigation
    >
      <Spinner isLoading={isLoading} />
      <S.PantryContainer>
        <VerticalList
          emptyMessage="Adicione itens na seção de itens da despensa para que eles apareçam por
            aqui!"
          itemList={pantryItems}
          listTitle="Itens da despensa"
          listType="pantryItem"
          listFooter={pantryItemListFooter()}
        />
        <VerticalList
          emptyMessage=" Adicione itens na seção de lista de compras para que eles
                apareçam por aqui!"
          listTitle="Lista de compras"
          itemList={shopLists}
          listType="shopList"
          listFooter={shopListFooter()}
          refetch={fetchHouseAllPantry}
        />
      </S.PantryContainer>
    </MainPage>
  )
}

export default PantryGeneral
