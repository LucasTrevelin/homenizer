import styled from 'styled-components'

interface IInventoryGrid {
  cardLength: number
}

export const InventoryContainer = styled.div<IInventoryGrid>`
  display: ${({ cardLength }) => (cardLength <= 4 ? 'flex' : 'grid')};
  grid-gap: 16px;
  padding: 16px;
  row-gap: 16px;
  grid-template-columns: ${({ cardLength }) =>
    cardLength > 4 && 'repeat(auto-fill, minmax(250px, 1fr))'};
  grid-template-rows: ${({ cardLength }) => cardLength > 4 && '1fr 1fr'};
  justify-content: space-around;
  align-items: center;
  grid-auto-flow: ${({ cardLength }) => cardLength > 4 && 'row'};
  grid-auto-columns: ${({ cardLength }) =>
    cardLength > 4 && 'minmax(240px, 1fr)'};
  overflow-y: auto;
  max-width: 85vw;
  box-shadow: 10px 0 10px 0px rgba(0 0 0 / 0.5);
  min-height: 55.5vh;
  max-height: 55.5vh;
  width: 100%;
`

export const EmptyList = styled.div`
  width: 100%;
  height: 100%;
  margin-top: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const ItemAreaContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  row-gap: 1.5rem;
`

export const DummySpaceButton = styled.div`
  width: 10rem;
  height: 2rem;
`
