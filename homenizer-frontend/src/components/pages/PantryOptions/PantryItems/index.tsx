import { AxiosError } from 'axios'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { toast } from 'react-toastify'
import NewPantryItemModal from '../../../../containers/NewPantryItemModal'
import {
  IPantryItemRequest,
  IPantryItemResponse
} from '../../../../models/PantryItem.model'
import {
  createPantryItem as createPantryItem,
  deletePantryItemById,
  getHousePantryItems
} from '../../../../services/PantryItem.service'
import { Button } from '../../../Button'
import Card from '../../../Card'
import MainPage from '../../../MainPage'
import Spinner from '../../../Spinner'
import * as S from './styles'

const PantryItems: React.FC = () => {
  const [pantryItems, setPantryItems] = useState<IPantryItemResponse[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const [addModalOpen, setAddModalOpen] = useState(false)

  const savedHouse = JSON.parse(localStorage.getItem('user-house')!)

  const fetchHouseNotes = useCallback(async () => {
    setIsLoading(true)
    if (savedHouse._id)
      await getHousePantryItems(savedHouse._id)
        .then((response) => {
          if (response.status === 200 && !response.data.length) {
            toast.warning(
              'Ainda não existem itens criados na despensa desta casa.',
              { toastId: 'fetch-house-pantry-items-warning-2' }
            )
          }
          setPantryItems(response.data)
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          return toast.error(
            (error as AxiosError).message === 'Network Error'
              ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
              : (
                  (error as AxiosError).response?.data as {
                    message: string
                  }
                ).message,
            { toastId: 'fetch-house-pantry-items-error' }
          )
        })

    setIsLoading(false)
  }, [])

  const handlePantryItemCreation = async (
    newItem: IPantryItemRequest,
    onClose: () => void,
    setLoadingModalState: (isLoading: boolean) => void
  ) => {
    if (savedHouse) {
      setIsLoading(true)
      try {
        const response = await createPantryItem({
          ...newItem
        })
        setIsLoading(false)
        toast.success(response.data.message, {
          toastId: 'pantry-item-creation-success'
        })
        await onClose()
        fetchHouseNotes()
      } catch (error) {
        setIsLoading(false)
        setLoadingModalState(false)
        return toast.error(
          (error as AxiosError).message === 'Network Error'
            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
            : (
                (error as AxiosError).response?.data as {
                  message: string
                }
              ).message,
          { toastId: 'pantry-item-creation-error' }
        )
      }
    }
  }

  const handleRemoveItem = async (_id: string) => {
    try {
      const response = await deletePantryItemById(_id, savedHouse._id)
      if (response.status === 202) {
        fetchHouseNotes()
        return toast.success((response.data as { message: string }).message)
      }
    } catch (error) {
      return toast.error(
        (error as AxiosError).message === 'Network Error'
          ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
          : (
              (error as AxiosError).response?.data as {
                message: string
              }
            ).message
      )
    }
  }

  useEffect(() => {
    fetchHouseNotes()
  }, [])

  return (
    <MainPage
      title="Itens da despensa"
      subTitle="Adicione, remova e edite os itens da despensa."
      assetPath={'pantry_banner'}
      headerHasNavigation
    >
      <S.InventoryContainer cardLength={pantryItems?.length || 0}>
        <S.ItemAreaContainer>
          <Card type="addPantryItem" onClick={() => setAddModalOpen(true)} />
          <S.DummySpaceButton />
        </S.ItemAreaContainer>
        <NewPantryItemModal
          isOpen={addModalOpen}
          setIsOpen={setAddModalOpen}
          onSave={handlePantryItemCreation}
        />
        <Spinner isLoading={isLoading} />

        {pantryItems?.length ? (
          pantryItems?.map((pantryItem) => (
            <S.ItemAreaContainer>
              <Card
                type="pantryItem"
                key={`house-user-${pantryItem._id}`}
                setIsLoading={setIsLoading}
                refetch={fetchHouseNotes}
                pantryItem={pantryItem}
              />
              <Button
                title="Remover item"
                variant="danger"
                width="10rem"
                height="2rem"
                onClick={() => {
                  handleRemoveItem(pantryItem._id)
                }}
              />
            </S.ItemAreaContainer>
          ))
        ) : (
          <S.EmptyList>
            Adicione itens na seção de itens da despensa para que eles apareçam
            por aqui!
          </S.EmptyList>
        )}
      </S.InventoryContainer>
    </MainPage>
  )
}

export default PantryItems
