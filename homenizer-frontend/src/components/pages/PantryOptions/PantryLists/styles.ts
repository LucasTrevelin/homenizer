import styled from 'styled-components'

export const Container = styled.div`
  height: 100vh;
  max-width: 100vw;
  width: 100%;
  display: flex;
  flex-direction: column;
  max-height: 55.5vh;
`

export const Content = styled.div`
  display: flex;
  height: 100%;
`

export const PageContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

export const TitleSection = styled.div`
  margin-top: 2rem;
  width: 100%;
  text-align: center;
  & > p {
    font-size: 1.5rem;
    font-weight: 400;
  }
`

export const PantryContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 100%;
  max-height: 60vh;
  box-shadow: 10px 0 10px 0px rgba(0 0 0 / 0.5);
`

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`
export const EmptyListButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  margin-right: 3rem;
`

export const EmptyListContainer = styled.div`
  width: 100%;
  height: 100%;
  padding: 2rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  text-align: center;
`

export const EmptyListMessageContainer = styled.div`
  width: 100%;
  height: 60%;
  display: flex;
  justify-content: center;
  align-items: center;
`
