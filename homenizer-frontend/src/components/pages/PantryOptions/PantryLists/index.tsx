import { AxiosError } from 'axios'
import React, { useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import ShopListModal, {
  EShopListModalType
} from '../../../../containers/ShopListModal'
import { IShopListResponse } from '../../../../models/ShopList.model'
import { getHouseShopLists } from '../../../../services/ShopList.service'
import { Button } from '../../../Button'
import FullPageList from '../../../FullPageList'
import { ListComponent } from '../../../ListComponent'
import MainPage from '../../../MainPage'
import Spinner from '../../../Spinner'
import * as S from './styles'

const PantryLists: React.FC = () => {
  const [isOpenAddModal, setIsOpenAddModal] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [shopLists, setShopLists] = useState<IShopListResponse[]>([])

  const savedHouse = JSON.parse(localStorage.getItem('user-house')!)

  const fetchShopLists = useCallback(async () => {
    setIsLoading(true)
    if (savedHouse._id)
      await getHouseShopLists(savedHouse._id)
        .then((response) => {
          if (response.status === 200 && !response.data.length) {
            toast.warning(
              'Ainda não existem listas criadas na despensa desta casa.',
              { toastId: 'fetch-house-shop-lists-warning-2' }
            )
          }
          setShopLists(response.data)
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          return toast.error(
            (error as AxiosError).message === 'Network Error'
              ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
              : (
                  (error as AxiosError).response?.data as {
                    message: string
                  }
                ).message,
            { toastId: 'fetch-house-pantry-items-error' }
          )
        })

    setIsLoading(false)
  }, [])

  const pageFooter = () => {
    return (
      <S.EmptyListButtonContainer>
        <Button
          variant={'filled'}
          title={'Adicionar Lista'}
          onClick={() => setIsOpenAddModal(true)}
        />
      </S.EmptyListButtonContainer>
    )
  }

  useEffect(() => {
    fetchShopLists()
  }, [])

  return (
    <MainPage
      title="Listas de compras"
      subTitle="Escreva e organize suas listas de compras"
      assetPath={'pantry_banner'}
      headerHasNavigation
    >
      <Spinner isLoading={isLoading} />
      <S.Container>
        <FullPageList
          columnWidth="432px"
          rowWidth="87px"
          itemsLength={shopLists.length}
          maxItemsFlex={5}
          flexDirection={'column'}
          maxHeight={'45vh'}
          footer={pageFooter()}
        >
          {shopLists.length ? (
            shopLists.map((shopList, index) => (
              <ListComponent
                key={`shop-list-${`item-${shopList._id}`}`}
                item={shopList}
                type={'shopList'}
                refetch={fetchShopLists}
              />
            ))
          ) : (
            <>
              <S.EmptyListContainer>
                <S.EmptyListMessageContainer>
                  Ainda não existem listas de compras criadas.
                  <br />
                  <br />
                  Crie listas de compras para que elas apareçam por aqui!
                </S.EmptyListMessageContainer>
              </S.EmptyListContainer>
            </>
          )}
        </FullPageList>
      </S.Container>
      {isOpenAddModal && (
        <ShopListModal
          isOpen={isOpenAddModal}
          setIsOpen={setIsOpenAddModal}
          refetch={fetchShopLists}
          type={EShopListModalType.NEW}
        />
      )}
    </MainPage>
  )
}

export default PantryLists
