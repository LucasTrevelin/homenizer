import styled from 'styled-components'

export const Container = styled.div`
  max-width: 100%;
  height: 100vh;
  background-color: ${({ theme }) => theme.colors.backgroundPrimary};
  display: flex;
  flex-direction: column;
  flex: 1;
`

export const Content = styled.div`
  height: 100%;
  width: 100%;
  padding: 2rem 0;
  text-align: center;
  display: flex;
  flex-direction: column;

  & > p {
    font-size: 1.5rem;
  }
`

export const OptionsContainer = styled.div`
  margin: 4rem auto;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr 1fr;
  gap: 2rem;
`
