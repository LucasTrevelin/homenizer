import React, { useCallback, useEffect, useState } from 'react'
import NewHomeModal from '../../../containers/NewHomeModal'
import SearchHomeModal from '../../../containers/SearchHomeModal'
import { IHouseResponse } from '../../../models/House.model'
import { getAllHouses } from '../../../services/House.service'
import { Button } from '../../Button'
import Header from '../../Header'

import { Container, Content, OptionsContainer } from './styles'

const Options: React.FC = () => {
  const [modalIsOpen, setIsModalOpen] = useState({ new: false, search: false })
  const [houses, setHouses] = useState<IHouseResponse[]>()

  const fetchHouses = useCallback(async () => {
    const response = await getAllHouses()
    if (response) setHouses(response.data)
  }, [setHouses])

  useEffect(() => {
    fetchHouses()
  }, [])

  return (
    <Container>
      <Header />
      <Content>
        <NewHomeModal
          isOpen={modalIsOpen.new}
          setIsOpen={() => setIsModalOpen({ ...modalIsOpen, new: false })}
        />
        <SearchHomeModal
          isOpen={modalIsOpen.search}
          setIsOpen={() => setIsModalOpen({ ...modalIsOpen, search: false })}
          availableHouses={houses}
        />
        <>
          <h1>Casa</h1>
          <p>Crie uma nova ou encontre uma casa já criada</p>
        </>
        <OptionsContainer>
          <Button
            onClick={() => setIsModalOpen({ ...modalIsOpen, new: true })}
            height="6.25rem"
            fontSize="1rem"
            width="28rem"
            variant="filled"
            title="Adicionar nova casa"
          />
          <Button
            onClick={() => setIsModalOpen({ ...modalIsOpen, search: true })}
            height="6.25rem"
            fontSize="1rem"
            width="28rem"
            variant="filled"
            title="Procurar nova casa"
            disabled={!houses?.length}
          />
        </OptionsContainer>
      </Content>
    </Container>
  )
}

export default Options
