import React from 'react'
import { useLocation } from 'react-router-dom'
import Board from './Board'
import HomeFavorites from './HomeFavorites'
import HomeWriteNote from './HomeWriteNote'

const HomeOptions: React.FC = () => {
  const location = useLocation()

  switch (location.pathname) {
    case '/home':
      return <Board />

    case '/home/favorites':
      return <HomeFavorites />

    case '/home/write':
      return <HomeWriteNote />

    default:
      return <></>
  }
}

export default HomeOptions
