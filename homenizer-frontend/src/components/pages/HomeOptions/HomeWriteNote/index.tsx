import React from 'react'
import { createNote } from '../../../../services/Note.service'
import MainPage from '../../../MainPage'
import PostIt, { EPostIt } from '../../../PostIt'
import { PostsContainer } from './styles'

const HomeWriteNote: React.FC = () => {
  return (
    <MainPage
      title="Mural"
      subTitle="Escreva um novo recado ou aviso"
      assetPath={'board_banner'}
      headerHasNavigation
    >
      <PostsContainer>
        <PostIt
          type={EPostIt.creating}
          onSave={createNote}
          data-testid="writable-note"
        />
      </PostsContainer>
    </MainPage>
  )
}

export default HomeWriteNote
