import { AxiosError } from 'axios'
import React, { useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { INoteResponse } from '../../../../models/Note.model'
import { IUserResponse } from '../../../../models/User.model'
import { getHouseNotes, updateNote } from '../../../../services/Note.service'
import FullPageList from '../../../FullPageList'
import MainPage from '../../../MainPage'
import PostIt, { EPostIt } from '../../../PostIt'
import Spinner from '../../../Spinner'
import * as S from './styles'

const Board: React.FC = () => {
  const [notes, setNotes] = useState<INoteResponse[]>([])
  const [isLoading, setIsLoading] = useState(false)

  const fetchHouseNotes = useCallback(async () => {
    const authenticatedUser = await JSON.parse(
      localStorage.getItem('user-house')!
    )
    setIsLoading(true)
    if (authenticatedUser._id)
      await getHouseNotes(authenticatedUser._id)
        .then((response) => {
          if (response.status === 200) {
            toast.warning((response.data as { message: string }).message)
          }
          setNotes(response.data as INoteResponse[])
          setIsLoading(false)
        })
        .catch((error) => {
          setIsLoading(false)
          return toast.error(
            (error as AxiosError).message === 'Network Error'
              ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
              : (
                  (error as AxiosError).response?.data as {
                    message: string
                  }
                ).message
          )
        })

    setIsLoading(false)
  }, [])

  useEffect(() => {
    fetchHouseNotes()
  }, [fetchHouseNotes])

  return (
    <>
      <Spinner isLoading={isLoading} />
      <MainPage
        title="Mural"
        subTitle="Todos os avisos e recados"
        assetPath={'board_banner'}
        headerHasNavigation
      >
        <FullPageList
          itemsLength={notes.length}
          columnWidth={'470px'}
          rowWidth={'189px'}
          maxItemsFlex={3}
        >
          {notes.length ? (
            notes?.map((note) => (
              <PostIt
                key={`post-it-${note._id}`}
                note={note}
                onUpdate={updateNote}
                refetch={fetchHouseNotes}
                type={EPostIt.reading}
              />
            ))
          ) : (
            <S.EmptyBoard>
              Crie recados e/ou avisos para serem exibidos aqui. Todos da sua
              casa tem acesso!
            </S.EmptyBoard>
          )}
        </FullPageList>
      </MainPage>
    </>
  )
}

export default Board
