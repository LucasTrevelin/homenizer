import styled from 'styled-components'

export const Container = styled.div`
  height: 100vh;
  max-width: 100vw;
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Content = styled.div`
  display: flex;
  height: 100%;
`

export const PageContent = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`

export const TitleSection = styled.div`
  margin-top: 2rem;
  width: 100%;
  text-align: center;
  & > p {
    font-size: 1.5rem;
    font-weight: 400;
  }
`
export const PostItContainer = styled.div<{ notesLength: number }>`
  display: ${({ notesLength }) => (notesLength <= 3 ? 'flex' : 'grid')};
  grid-gap: 1rem;
  padding: 1rem;
  row-gap: 1rem;
  grid-template-columns: ${({ notesLength }) =>
    notesLength > 3 && 'repeat(auto-fill, minmax(400px, 1fr))'};
  grid-template-rows: ${({ notesLength }) => notesLength > 3 && '1fr 1fr'};
  justify-content: center;
  align-items: center;
  grid-auto-flow: ${({ notesLength }) => notesLength > 3 && 'row'};
  grid-auto-columns: ${({ notesLength }) =>
    notesLength > 4 && 'minmax(300px, 1fr)'};
  overflow-y: auto;
  max-width: 85.3vw;
  box-shadow: 10px 0 10px 0px rgba(0 0 0 / 0.5);
  min-height: 55.5vh;
  max-height: 55.5vh;
  width: 100%;
  justify-items: center;
`
export const EmptyBoard = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`
