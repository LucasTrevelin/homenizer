import React, { useCallback, useEffect, useState } from 'react'
import { IHouseResponse } from '../../../../models/House.model'
import { INoteResponse } from '../../../../models/Note.model'
import {
  getAllNotes,
  getHouseNotes,
  updateNote
} from '../../../../services/Note.service'
import MainPage from '../../../MainPage'
import PostIt, { EPostIt } from '../../../PostIt'
import * as S from '../Board/styles'

const HomeFavorites: React.FC = () => {
  const [favoriteNotes, setFavoriteNotes] = useState<INoteResponse[]>([])

  const savedHouse: IHouseResponse = localStorage.getItem('user-house')
    ? JSON.parse(localStorage.getItem('user-house')!)
    : ''

  const fetchFavoriteNotes = useCallback(async () => {
    const response = await getHouseNotes(savedHouse._id, true)
    setFavoriteNotes(response.data as INoteResponse[])
  }, [])

  useEffect(() => {
    fetchFavoriteNotes()
  }, [fetchFavoriteNotes])

  return (
    <MainPage
      title="Mural"
      subTitle="Leia ou relembre os favoritos"
      assetPath={'board_banner'}
      headerHasNavigation
    >
      <S.PostItContainer notesLength={favoriteNotes.length}>
        {favoriteNotes.length ? (
          favoriteNotes?.map((note) => (
            <PostIt
              key={`post-it-${note._id}`}
              note={note}
              onUpdate={updateNote}
              refetch={fetchFavoriteNotes}
              type={EPostIt.favorite}
            />
          ))
        ) : (
          <S.EmptyBoard>
            Não há recados ou avisos favoritados neste momento!
          </S.EmptyBoard>
        )}
      </S.PostItContainer>
    </MainPage>
  )
}

export default HomeFavorites
