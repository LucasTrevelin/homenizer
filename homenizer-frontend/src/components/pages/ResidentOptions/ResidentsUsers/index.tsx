import React, { useEffect, useState } from 'react'
import UserDetails from '../../../../containers/ModalUserDetails'
import { IHouseResponse } from '../../../../models/House.model'
import { IUserResponse } from '../../../../models/User.model'
import {
  getHouseAdministrators,
  getHouseResidents
} from '../../../../services/User.service'
import Card from '../../../Card'
import MainPage from '../../../MainPage'
import Spinner from '../../../Spinner'
import * as S from '../Residents/styles'

const ResidentsUsers = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [users, setUsers] = useState<IUserResponse[]>()
  const [loading, setLoading] = useState(false)
  const [resident, setResident] = useState<IUserResponse>()

  const openModal = (user: IUserResponse) => {
    setResident(user)
    setModalIsOpen(true)
  }

  const savedHouse = JSON.parse(
    localStorage.getItem('user-house')!
  ) as IHouseResponse

  const fetchHouseAdministrators = async () => {
    setLoading(true)
    const response = await getHouseAdministrators(savedHouse._id, 'user')
    setLoading(false)
    setUsers(response.data)
  }

  useEffect(() => {
    fetchHouseAdministrators()
  }, [])

  useEffect(() => {
    if (!modalIsOpen) setResident(undefined)
  }, [modalIsOpen])

  return (
    <>
      <Spinner isLoading={loading} />
      <MainPage
        title="Moradores"
        subTitle="Adicione, exclua ou modifique o status dos moradores"
        assetPath={'residents_banner'}
        headerHasNavigation
      >
        <S.ResidentsContainer cardLength={users?.length || 0}>
          {users?.length ? (
            users?.map((user) => (
              <>
                <Card
                  key={`house-user-${user._id}`}
                  onClick={() => openModal(user)}
                  user={user}
                />
              </>
            ))
          ) : (
            <S.EmptyResidents>
              Crie recados e/ou avisos para serem exibidos aqui. Todos da sua
              casa tem acesso!
            </S.EmptyResidents>
          )}
        </S.ResidentsContainer>
        {resident ? (
          <UserDetails
            isOpen={modalIsOpen}
            setIsOpen={setModalIsOpen}
            user={resident}
            refetch={fetchHouseAdministrators}
          />
        ) : (
          <></>
        )}
      </MainPage>
    </>
  )
}

export default ResidentsUsers
