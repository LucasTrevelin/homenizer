import React from 'react'
import { useLocation } from 'react-router-dom'
import Residents from './Residents'
import ResidentsAdministrators from './ResidentsAdministrators'
import ResidentsUsers from './ResidentsUsers'

const ResidentOptions: React.FC = () => {
  const location = useLocation()

  switch (location.pathname) {
    case '/residents':
      return <Residents />

    case '/residents/administrators':
      return <ResidentsAdministrators />

    case '/residents/users':
      return <ResidentsUsers />

    default:
      return <></>
  }
}

export default ResidentOptions
