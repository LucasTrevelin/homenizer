import { AxiosError } from 'axios'
import React, { Fragment, useEffect, useState } from 'react'
import { Navigate, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import ModalUserDetails from '../../../../containers/ModalUserDetails'
import { IHouseResponse } from '../../../../models/House.model'
import { IUserResponse } from '../../../../models/User.model'
import { getHouseResidents } from '../../../../services/User.service'
import Card from '../../../Card'
import MainPage from '../../../MainPage'
import Spinner from '../../../Spinner'
import * as S from './styles'

const Residents = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false)
  const [users, setUsers] = useState<IUserResponse[]>()
  const [loading, setLoading] = useState(false)
  const [resident, setResident] = useState<IUserResponse>()

  const navigate = useNavigate()

  const openModal = (user: IUserResponse) => {
    setResident(user)
    setModalIsOpen(true)
  }

  const savedHouse = JSON.parse(
    localStorage.getItem('user-house')!
  ) as IHouseResponse

  const renderPageContent = () => {
    if (users?.length && !loading) {
      return users?.map((user) => (
        <>
          <Card
            key={`house-user-${user._id}`}
            onClick={() => openModal(user)}
            user={user}
          />
        </>
      ))
    } else return <></>
  }

  const fetchHouseResidents = async () => {
    setLoading(true)
    try {
      const response = await getHouseResidents(savedHouse._id)
      setLoading(false)
      setUsers(response.data)
    } catch (error) {
      console.log((error as AxiosError).response?.data)
      if (
        ((error as AxiosError).response?.data as { message: string })
          .message === 'Casa não encontrada!'
      ) {
        navigate('/options')
        toast.warning(
          'Esta casa foi removida por não haver mais residentes. por favor escolha uma casa existente ou crie uma nova.',
          { toastId: 'all-residents-removed-warning' }
        )
      }
    }
  }

  useEffect(() => {
    fetchHouseResidents()
  }, [])

  useEffect(() => {
    if (!modalIsOpen) setResident(undefined)
  }, [modalIsOpen])

  return (
    <>
      <Spinner isLoading={loading} />
      <MainPage
        title="Moradores"
        subTitle="Adicione, exclua ou modifique o status dos moradores"
        assetPath={'residents_banner'}
        headerHasNavigation
      >
        <S.ResidentsContainer cardLength={users?.length || 0}>
          {renderPageContent()}
        </S.ResidentsContainer>
        {resident ? (
          <ModalUserDetails
            isOpen={modalIsOpen}
            setIsOpen={setModalIsOpen}
            user={resident}
            refetch={fetchHouseResidents}
          />
        ) : (
          <S.EmptyResidents>
            Crie recados e/ou avisos para serem exibidos aqui. Todos da sua casa
            tem acesso!
          </S.EmptyResidents>
        )}
      </MainPage>
    </>
  )
}

export default Residents
