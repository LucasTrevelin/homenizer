import styled from 'styled-components'

interface IResidentsGrid {
  cardLength: number
}

export const ResidentsContainer = styled.div<IResidentsGrid>`
  display: ${({ cardLength }) => (cardLength <= 4 ? 'flex' : 'grid')};
  grid-gap: 16px;
  padding: 16px;
  row-gap: 16px;
  grid-template-columns: ${({ cardLength }) =>
    cardLength > 4 && 'repeat(auto-fill, minmax(300px, 1fr))'};
  grid-template-rows: ${({ cardLength }) => cardLength > 4 && '1fr 1fr'};
  justify-content: space-around;
  align-items: center;
  grid-auto-flow: ${({ cardLength }) => cardLength > 4 && 'row'};
  grid-auto-columns: ${({ cardLength }) =>
    cardLength > 4 && 'minmax(300px, 1fr)'};
  overflow-y: auto;
  max-width: 85vw;
  box-shadow: 10px 0 10px 0px rgba(0 0 0 / 0.5);
  min-height: 55.5vh;
  max-height: 55.5vh;
  width: 100%;
`

export const EmptyCard = styled.div`
  aspect-ratio: 1/1;
  height: 240px;
`
export const EmptyResidents = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`
