import { AxiosError } from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import { UserContext } from '../../context/User.context'
import { IHouseResponse } from '../../models/House.model'
import { INoteRequest, INoteResponse } from '../../models/Note.model'
import { IUserResponse } from '../../models/User.model'
import { deleteNote, updateNote } from '../../services/Note.service'
import { Response } from '../../services/RequestService'

import * as S from './styles'

export const enum EPostIt {
  reading = 'READING',
  favorite = 'favorite',
  creating = 'CREATING'
}
interface PostItProps {
  type: EPostIt
  note?: INoteResponse
  onSave?: (noteRequest: INoteRequest) => Promise<Response<unknown>>
  refetch?: () => Promise<void>
  onUpdate?: (_id: string, changes?: unknown) => Promise<Response<unknown>>
}

const PostIt: React.FC<PostItProps> = ({
  type,
  note,
  onSave,
  onUpdate,
  refetch
}) => {
  const { message, author, favorite, _id, editionAuthor } = note || {}

  const [edit, setEdit] = useState(type !== EPostIt.creating ? false : true)
  const [text, setText] = useState(message || '')
  const [isFavorite, setIsFavorite] = useState(favorite)

  const { loggedUser } = useContext(UserContext)

  const savedHouse: IHouseResponse = localStorage.getItem('user-house')
    ? JSON.parse(localStorage.getItem('user-house')!)
    : ''

  const savedUser = JSON.parse(
    localStorage.getItem('homenizer-app')!
  ) as IUserResponse

  const ASCIISum = (string: string) => {
    const isPrime = (n: number) => {
      if (n == 0 || n == 1) return false
      for (let i = 2; i * i <= n; i++) if (n % i == 0) return false

      return true
    }

    // To store the sum
    let sum = 0

    // For every character
    for (let i = 0; i < author!.length; i++) {
      // If current position is prime
      // then add the ASCII value of the
      // character at the current position
      if (isPrime(i + 1)) sum += string.charCodeAt(i)
    }

    if (sum % 2) return '0'
    if (sum % 3) return '1'
    return '2'
  }

  const colorCode = () => {
    if (author) {
      return String(ASCIISum(author)) as '0' | '1' | '2' | '3'
    }
    return '0'
  }

  return (
    <>
      <S.Container postItColor={colorCode()}>
        <S.PostItContent>
          <S.MessageLine>
            <S.Input
              rows={3}
              value={text || ''}
              disabled={!edit}
              onChange={({ target }) => setText(target.value)}
              autoFocus={edit}
              isCreating={type === EPostIt.creating ? true : false}
            />

            {type !== EPostIt.creating ? (
              <img
                alt="favoriting-a-post-it"
                src={`/assets/icons/${
                  !isFavorite ? 'favorite-icon' : 'favorite-icon-filled'
                }.png`}
                onClick={async () => {
                  try {
                    if (_id)
                      await updateNote(_id, {
                        favorite: !isFavorite,
                        editionAuthor:
                          loggedUser?.username || savedUser.username
                      })
                    setIsFavorite(!isFavorite)
                    refetch && refetch()
                  } catch (error) {
                    toast.error(
                      (error as AxiosError).message === 'Network Error'
                        ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                        : (
                            (error as AxiosError).response?.data as {
                              message: string
                            }
                          ).message
                    )
                  }
                }}
              />
            ) : undefined}
          </S.MessageLine>
          <S.PostItFooter>
            {author ? (
              <S.AuthoritiesFooter>
                <span>{`De: ${author}`}</span>
                {editionAuthor ? (
                  <span>{`Editado por: ${editionAuthor}`}</span>
                ) : (
                  <></>
                )}
              </S.AuthoritiesFooter>
            ) : (
              <span>{`De: ${loggedUser?.username || savedUser.username}`}</span>
            )}
            <S.ActionContainer>
              <img
                alt="edit-or-cancel-edition-post-it"
                src={`/assets/icons/${!edit ? 'edit-icon' : 'delete-icon'}.png`}
                onClick={() => {
                  if (message) {
                    setEdit(!edit)
                    if (edit) setText(message)
                  } else {
                    if (text) setText('')
                  }
                }}
                style={{ width: '24px', height: '24px' }}
              />

              <img
                alt="delete-or-save-edition-post-it"
                src={`/assets/icons/${!edit ? 'trash-icon' : 'save-icon'}.png`}
                onClick={async () => {
                  if (message) setEdit(false)
                  if (text && edit) {
                    if (onSave) {
                      try {
                        const response = await onSave({
                          author: loggedUser?.username || savedUser.username,
                          favorite: false,
                          message: text,
                          houseId: savedHouse._id
                        })
                        if (response.status === 201) {
                          setText('')
                          return toast.success('Recado criado com sucesso!')
                        }
                      } catch (error) {
                        return toast.error(
                          (error as AxiosError).message === 'Network Error'
                            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                            : (
                                (error as AxiosError).response?.data as {
                                  message: string
                                }
                              ).message
                        )
                      }
                    }
                    if (onUpdate) {
                      try {
                        if (_id && text) {
                          const response = await onUpdate(_id, {
                            message: text,
                            editionAuthor:
                              loggedUser?.username || savedUser.username
                          })
                          if (response.status === 201) {
                            refetch && refetch()
                            return toast.success(
                              'Recado atualizado com sucesso!'
                            )
                          }
                        }
                      } catch (error) {
                        return toast.error(
                          (error as AxiosError).message === 'Network Error'
                            ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                            : (
                                (error as AxiosError).response?.data as {
                                  message: string
                                }
                              ).message
                        )
                      }
                    }
                  }

                  if (_id && !edit) {
                    try {
                      const response = await deleteNote(_id, savedHouse._id)
                      if (response.status === 202) {
                        refetch && refetch()
                        return toast.success(
                          (response.data as { message: string }).message
                        )
                      }
                    } catch (error) {
                      return toast.error(
                        (error as AxiosError).message === 'Network Error'
                          ? 'Houve um problema com a conexão. Por favor, tente novamente mais tarde!'
                          : (
                              (error as AxiosError).response?.data as {
                                message: string
                              }
                            ).message
                      )
                    }
                  }
                }}
              />
            </S.ActionContainer>
          </S.PostItFooter>
        </S.PostItContent>
      </S.Container>
    </>
  )
}

export default PostIt
