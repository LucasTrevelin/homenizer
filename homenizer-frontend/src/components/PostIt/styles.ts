import styled, { css } from 'styled-components'

const PostItColors = {
  0: css`
    background-color: ${({ theme }) => theme.colors.backgroundYellow};
  `,
  1: css`
    background-color: ${({ theme }) => theme.colors.backgroundGreen};
  `,
  2: css`
    background-color: ${({ theme }) => theme.colors.backgroundOrange};
  `,
  3: css`
    background-color: ${({ theme }) => theme.colors.backgroundBlue};
  `
}

interface ContainerProps {
  postItColor: '0' | '1' | '2' | '3'
}

export const Container = styled.div<ContainerProps>`
  height: 8.8rem;
  aspect-ratio: 3/1;
  display: flex;
  justify-content: flex-start;
  padding: 1.5rem;
  text-align: center;
  border-radius: 10px;
  ${({ postItColor }) => PostItColors[postItColor]};

  font-size: 1.5rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.tertiary};
  img {
    cursor: pointer;
  }
`

export const PostItContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  text-align: start;
`

export const AuthoritiesFooter = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 5px;
`
export const MessageLine = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  text-align: start;
`

export const PostItFooter = styled.div`
  font-size: 0.875rem;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const ActionContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 0.5rem;
`

export const Input = styled.textarea<{ isCreating: boolean }>`
  background: transparent;
  width: ${({ isCreating }) => (isCreating ? '100%' : '90%')};
  border: ${({ disabled, theme }) =>
    !disabled ? `1px solid #000000` : 'none'};

  font-size: 1.5rem;
  font-weight: 500;
  color: ${({ theme }) => theme.colors.tertiary};
  overflow-y: auto;
  resize: none;
`
