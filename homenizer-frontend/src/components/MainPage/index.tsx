import React, { ReactNode } from 'react'
import { useLocation } from 'react-router-dom'
import { sideBarOptions } from '../../mappers/sideBarOptions.mapper'
import { EURLs } from '../../mappers/URL.mapper'
import Banner from '../Banner'
import Header from '../Header'
import SideBar from '../SideBar'
import * as S from './styles'

interface MainPageProps {
  title: string
  subTitle: string
  children: ReactNode
  assetPath: string
  headerHasNavigation?: boolean
}

const MainPage: React.FC<MainPageProps> = ({
  title,
  subTitle,
  children,
  assetPath,
  headerHasNavigation
}) => {
  const location = useLocation()

  return (
    <S.Container>
      <Header hasNavigation={headerHasNavigation} />
      <S.Content>
        <SideBar options={sideBarOptions[location.pathname as EURLs].options} />
        <S.PageContent>
          {assetPath ? <Banner assetPath={assetPath} /> : <></>}
          <S.TitleSection>
            <h1>{title}</h1>
            <p>{subTitle}</p>
          </S.TitleSection>
          {children}
        </S.PageContent>
      </S.Content>
    </S.Container>
  )
}

export default MainPage
