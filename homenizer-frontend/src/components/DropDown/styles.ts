import styled from 'styled-components'

export const DropdownContainer = styled.div<{
  isVisible: boolean
  length: number
}>`
  position: absolute;
  top: 65%;
  right: 65%;
  display: ${({ isVisible }) => (isVisible ? 'visible' : 'none')}!important;
  width: 20rem;
  background-color: ${({ theme }) => theme.colors.textWhite};
  z-index: 9999999;
  border-radius: 1rem;
  box-shadow: 0 10px 10px #666;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`

export const DropdownOption = styled.button`
  width: 100%;
  padding: 1rem 0;
  text-align: center;
  display: flex;
  justify-content: center;
  color: ${({ theme }) => theme.colors.black};
  font-size: 18px;
  padding: 1rem;
  cursor: pointer;
  border: none;
  background-color: inherit;
  transition: all 0.1s ease-in-out;
  :hover {
    filter: brightness(0.8);
  }
`
export const HeaderContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  border-radius: 1rem 1rem 0;
  width: 100%;
  padding: 1rem 0;
`

export const Header = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  color: ${({ theme }) => theme.colors.black};
  font-size: 18px;
  padding: 1rem;
`

export const Footer = styled.div`
  text-align: center;
  display: flex;
  justify-content: center;
  color: ${({ theme }) => theme.colors.black};
  font-size: 14px;
  padding: 1rem;
`
