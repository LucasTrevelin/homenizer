import React from 'react'
import { IHouseResponse } from '../../models/House.model'
import AvatarProfile from '../AvatarProfile'
import Divider from '../Divider'
import * as S from './styles'

interface DropdownProps {
  isOpen: boolean
  options: DrodpownOption[]
  username: string
}
interface DrodpownOption {
  label: string
  onClick: () => void
}

export const Dropdown: React.FC<DropdownProps> = ({
  isOpen,
  options,
  username
}) => {
  const savedHouse: IHouseResponse = JSON.parse(
    localStorage.getItem('user-house')!
  )
  return (
    <S.DropdownContainer length={options.length} isVisible={isOpen}>
      <S.Header>{username}</S.Header>
      <Divider />
      {options.map((option, index) => {
        const { onClick, label } = option
        return (
          <S.DropdownOption
            key={`dropdown-option-${label}-${index}`}
            onClick={async () => onClick()}
          >
            {label}
          </S.DropdownOption>
        )
      })}
      <Divider />
      {savedHouse?.name ? (
        <S.Footer>{`Casa: ${savedHouse?.name}`}</S.Footer>
      ) : undefined}
    </S.DropdownContainer>
  )
}
