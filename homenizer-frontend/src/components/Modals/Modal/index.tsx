import { ReactNode } from 'react'
import Modal from 'react-modal'
import { theme } from '../../../theme/theme'

const getCustomStyles = (
  marginTop?: string,
  height?: string,
  width?: string
) => {
  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      backgroundColor: `${theme.colors.backgroundPrimary}`,
      height: height && height,
      width: width && width
    },
    overlay: {
      top: marginTop || '0',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
      zIndex: '999'
    }
  }

  return customStyles
}

interface ModalComponentProps {
  modalIsOpen: boolean
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  closeModal: any
  children?: ReactNode
  marginTop?: string
  height?: string
  width?: string
}

const ModalComponent = ({
  modalIsOpen,
  closeModal,
  children,
  marginTop,
  height,
  width
}: ModalComponentProps): JSX.Element => {
  return (
    <Modal
      isOpen={modalIsOpen}
      onAfterOpen={() => undefined}
      onRequestClose={closeModal}
      style={getCustomStyles(marginTop, width, height)}
      contentLabel="Example Modal"
      ariaHideApp={false}
    >
      {children}
    </Modal>
  )
}

export default ModalComponent
