import styled from 'styled-components'

export const Container = styled.div`
  height: fit-content;
  width: fit-content;
`

export const Select = styled.select`
  border-radius: 10px;
  width: 400px;
  height: 31px;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border: 1px solid ${({ theme }) => theme.colors.black};
  padding: 0 10px;
  margin-bottom: 20px;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.tertiary};
`

export const Option = styled.option`
  font-size: 18px;
  color: ${({ theme }) => theme.colors.tertiary};
`
