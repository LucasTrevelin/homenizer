import { AxiosError } from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import CreateUserModal from '../../containers/CreateUser'
import { UserContext, useUserContext } from '../../context/User.context'
import { validateEmail } from '../../helper/validateEmail.helper'
import { IHouseResponse } from '../../models/House.model'
import { userSignin } from '../../services/User.service'
import { Button } from '../Button'
import { Input } from '../Input'
import Spinner from '../Spinner'
import {
  BannerColumn,
  BannerContent,
  Container,
  ImageFrame,
  LoginColumn,
  LoginContent,
  StyledLink,
  Text,
  TextContainer,
  Advertisement,
  AdivertisementSubtitle,
  AdvertisementToDo,
  AdvertisementTexts,
  StyledForm
} from './styles'

export const Login: React.FC = () => {
  const navigate = useNavigate()
  // States
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [validation, setValidation] = useState({
    email: true,
    password: true
  })
  const [modalIsOpen, setModalIsOpen] = useState(false)

  const { saveAuthInfo } = useUserContext()

  const emailIsValid = validateEmail(email) || !email

  const handleClick = async () => {
    setIsLoading(true)
    try {
      const response = await userSignin({ email, password })
      await saveAuthInfo(response.data).then(() => {
        setIsLoading(false)
        toast.success(
          `Usuário logado com sucesso! Seja bem-vindo(a) ${response.data.user.username}!`
        )
        const savedHouse: IHouseResponse = JSON.parse(
          localStorage.getItem('user-house')!
        )
        if (savedHouse && savedHouse !== null) navigate('/home')
        else navigate('/options')
      })
    } catch (error) {
      setIsLoading(false)
      toast.error(
        ((error as AxiosError).response?.data as { message: string }).message
      )
    }
  }

  useEffect(() => console.log(validation), [validation])

  return (
    <Container>
      <BannerColumn>
        <BannerContent>
          <img
            alt="people-hugging-banner"
            src="assets/images/login_banner.png"
            width="100%"
          />
          <AdvertisementTexts>
            <Advertisement>
              Um espaço para que todos possam <br /> contribuir e conviver.
            </Advertisement>
            <AdivertisementSubtitle>
              <AdvertisementToDo>
                Conecte-se com as pessoas da sua casa
              </AdvertisementToDo>
              <AdvertisementToDo>Avisos para a galera</AdvertisementToDo>
              <AdvertisementToDo>
                Organize suas listas de compras
              </AdvertisementToDo>
              <AdvertisementToDo>
                Sem brigas ou atrasos: organize as contas da casa
              </AdvertisementToDo>
            </AdivertisementSubtitle>
          </AdvertisementTexts>
        </BannerContent>
      </BannerColumn>
      <LoginColumn>
        <ImageFrame>
          <img alt="logo" src="assets/images/logos/full_logo.png" />
        </ImageFrame>
        <TextContainer>
          <Text>Já é usuário ?</Text>
          <Text>Entre com suas credenciais</Text>
        </TextContainer>
        <LoginContent>
          <StyledForm>
            <Input
              placeholder="E-mail"
              state={email}
              setState={setEmail}
              name="e-mail"
              required={!validation.email}
              type="email"
              requiredMessage="E-mail é necessário estar no formato correto para login!"
            />
            <Input
              placeholder="Senha"
              margin="1rem 0 0 0"
              state={password}
              setState={setPassword}
              required={!validation.password}
              name="senha"
              type="password"
              requiredMessage="É necessário inserir uma senha para logar no sistema!"
            />
            <Button
              variant="filled"
              margin="2rem 0 0 0"
              title="Login"
              onClick={(event) => {
                event.preventDefault()
                if (emailIsValid && password) {
                  setValidation({ email: true, password: true })
                  handleClick()
                } else {
                  if (!emailIsValid && !password)
                    setValidation({ email: false, password: false })
                  if (emailIsValid && !password)
                    setValidation({ email: true, password: false })
                  if (!emailIsValid && password)
                    setValidation({ email: false, password: true })
                  toast.error(
                    'As credenciais informadas estão incorretas. Tente novamente!'
                  )
                }
              }}
            />
          </StyledForm>

          <CreateUserModal
            isOpen={modalIsOpen}
            setIsOpen={() => setModalIsOpen(false)}
          />
        </LoginContent>
        <TextContainer>
          <StyledLink disabled={true}>Esqueceu sua senha ?</StyledLink>
          <StyledLink onClick={() => setModalIsOpen(!modalIsOpen)}>
            Não tem conta ? Cadastre-se
          </StyledLink>
        </TextContainer>
      </LoginColumn>
      <Spinner isLoading={isLoading} />
    </Container>
  )
}
