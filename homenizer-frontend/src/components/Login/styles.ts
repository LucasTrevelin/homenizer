import styled, { css } from 'styled-components'

const Centralizer = css`
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Container = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
`

export const SpinnerContainer = styled.div`
  ${Centralizer}
  height: 100vh;
  width: 100vw;
  z-index: 1;
  position: fixed;
`

export const BannerColumn = styled.div`
  ${Centralizer};
  background-color: ${({ theme }) => theme.colors.backgroundPrimary};
  flex: 1;
`

export const BannerContent = styled.div`
  ${Centralizer};
  flex-direction: column;
  width: 100%;
`

export const AdvertisementTexts = styled.div`
  margin-top: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
`

export const Advertisement = styled.div`
  margin: 2rem 0;
  text-align: center;
  color: ${({ theme }) => theme.colors.textSecondary};
  font-size: 36px;
`

export const AdivertisementSubtitle = styled.div`
  text-align: center;
  color: ${({ theme }) => theme.colors.textSecondary};
  font-size: 18px;
`

export const AdvertisementToDo = styled.p`
  margin: 1rem 0;
  text-align: center;
  font-size: 18;
`

export const LoginColumn = styled.div`
  width: 40%;
  height: 100%;
  ${Centralizer};
  flex-direction: column;
`
export const ImageFrame = styled.div`
  ${Centralizer};
  margin-bottom: 2rem;
  width: 100%;
`

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 2rem;
`

export const Text = styled.div`
  margin: 0.5rem 0;
  display: inline-block;
  font-size: 18px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.tertiary};
`
export const LoginContent = styled.div`
  ${Centralizer}
  flex-direction: column;
  margin-bottom: 2rem;
`

export const StyledForm = styled.form`
  ${Centralizer}
  flex-direction: column;
`

export const StyledLink = styled.a<{ disabled?: boolean }>`
  margin: 0.5rem 0;
  display: inline-block;
  font-size: 18px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.tertiary};
  opacity: ${({ disabled }) => disabled && '0.5'};

  :hover {
    cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
    text-decoration: underline;
  }
`
