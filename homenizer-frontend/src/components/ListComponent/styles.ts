import styled from 'styled-components'
import { IVerticalList } from '../VerticalList'

interface IListContainerProps extends Pick<IVerticalList, 'listType'> {}

export const Container = styled.div<IListContainerProps>`
  box-shadow: 0 4px 4px 1px rgb(0 0 0 / 0.25);
  min-width: 486px;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 10px;
  padding: 6px 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  column-gap: 1rem;
  & > div > p {
    margin: 0;
  }
  margin: 0 2rem;
  min-height: '75px';
  transition: min-height 0.2s ease-in-out;
`

export const EmptyIcon = styled.div`
  aspect-ratio: 1/1;
  height: 38px;
`

export const ItemName = styled.p`
  font-size: 24px;
  font-weight: bold;
`

export const ItemQuantity = styled.p`
  font-size: 18px;
`

export const InfoContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-width: 100px;
`

export const BadgeContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  min-width: 86px;
`
export const ShopListTitle = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 0.5rem;
`

export const LastUpdate = styled.div`
  font-size: 14px;
  color: ${({ theme }) => theme.colors.textDisabled};
`

export const ShopListActionContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  column-gap: 0.5rem;
`

export const CorrectedImage = styled.img`
  margin-top: -2.5px;
  width: 28px;
  aspect-ratio: 1/1;
`

export const ImageChildren = styled.img``

export const Button = styled.button`
  aspect-ratio: 1/1;
  min-width: 50px;
  padding: 1;
  background-color: ${({ theme }) => theme.colors.textWhite};
  border-radius: 50%;
  border: transparent;
  outline: none;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  z-index: 4;
  transition: all 0.2s ease-in-out;

  :hover {
    transition: all 0.2s ease-in-out;
    cursor: pointer;
    filter: brightness(0.9);
  }
`
export const OptionsContainer = styled.div<{ openDetails: boolean }>`
  height: ${({ openDetails }) => (openDetails ? '100%' : '0px')};
  font-size: ${({ openDetails }) => !openDetails && '0px'};
  color: ${({ openDetails, theme }) => !openDetails && theme.colors.textWhite};
  display: flex;
  flex-direction: column;
  margin: 0 3rem;
  position: relative;
  overflow: hidden;
  transition: all 0.3s ease-in-out;
`
