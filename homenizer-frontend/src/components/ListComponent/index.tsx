import { AxiosError } from 'axios'
import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import ShopListModal, {
  EShopListModalType
} from '../../containers/ShopListModal'
import OptionRow from '../../containers/ShopListModal/OptionRow'
import { auxPantryIcons } from '../../helper/auxPantryIcons.helper'
import { itemsAvailability } from '../../helper/itemsAvailability.helper'
import { readDate } from '../../helper/readDate.helper'
import { unityDescriptionHasPlural } from '../../helper/unityDescriptionHasPlural.helper'
import { IHouseResponse } from '../../models/House.model'
import { IPantryItem } from '../../models/PantryItem.model'
import {
  IShopList,
  IShopListCreate,
  IShopListItemCreate,
  IShopListResponse
} from '../../models/ShopList.model'
import { deleteShopListById } from '../../services/ShopList.service'
import Badge from '../Badge'
import * as S from './styles'

export interface IListComponent {
  item: IShopList | IPantryItem
  type: 'shopList' | 'pantryItem'
  refetch?: () => Promise<void>
}

export const ListComponent: React.FC<IListComponent> = ({
  item,
  type,
  refetch
}) => {
  const [openDetails, setOpenDetails] = useState(false)

  const [editingShopList, setEditingShopList] = useState<IShopListCreate>(
    item as IShopListResponse
  )

  const savedHouse: IHouseResponse = JSON.parse(
    localStorage.getItem('user-house')!
  )

  const PantryItem = item as IPantryItem

  const auxIcon = auxPantryIcons(PantryItem.name)

  const pantryItemAvailabilty = itemsAvailability(PantryItem.quantity)

  const fixedCardInformation = () => {
    return (
      <>
        <S.ShopListTitle>
          <S.ItemName>{editingShopList.name}</S.ItemName>
          <S.LastUpdate>{`Última atualização: ${readDate(
            (item as IShopList).lastUpdate
          )}`}</S.LastUpdate>
        </S.ShopListTitle>
        <S.ShopListActionContainer>
          <S.Button onClick={() => setOpenDetails(!openDetails)}>
            <S.ImageChildren src="/assets/icons/list-icon.png" />
          </S.Button>
          <S.Button
            onClick={async () => {
              try {
                if (refetch) {
                  if (editingShopList._id) {
                    const response = await deleteShopListById(
                      editingShopList._id,
                      savedHouse._id
                    )
                    await refetch()
                    toast.success(response.data.message)
                  }
                }
              } catch (error) {
                toast.error(
                  ((error as AxiosError).response?.data as { message: string })
                    .message
                )
              }
            }}
          >
            <S.CorrectedImage src="/assets/icons/trash-icon.png" />
          </S.Button>
        </S.ShopListActionContainer>
      </>
    )
  }

  useEffect(() => {
    setEditingShopList(item as IShopList)
  }, [item as IShopList])

  return type === 'pantryItem' ? (
    <S.Container listType={type}>
      {auxIcon ? auxIcon : <S.EmptyIcon />}
      <S.InfoContainer>
        <S.ItemName>{item?.name}</S.ItemName>
      </S.InfoContainer>
      <S.InfoContainer>
        <S.ItemQuantity>
          {unityDescriptionHasPlural(PantryItem?.quantity)
            ? `${PantryItem?.quantity} unidades`
            : `${PantryItem?.quantity} unidade`}
        </S.ItemQuantity>
      </S.InfoContainer>
      <S.BadgeContainer>
        <Badge
          label={pantryItemAvailabilty!.label}
          color={pantryItemAvailabilty!.color}
        />
      </S.BadgeContainer>
    </S.Container>
  ) : (
    <>
      <S.Container listType={type}>{fixedCardInformation()}</S.Container>
      {openDetails && refetch ? (
        <ShopListModal
          isOpen={openDetails}
          setIsOpen={setOpenDetails}
          refetch={refetch}
          type={EShopListModalType.EDIT}
          shopList={editingShopList}
        />
      ) : (
        <></>
      )}
    </>
  )
}
