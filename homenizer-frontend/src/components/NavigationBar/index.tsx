import React from 'react'
import { useLocation } from 'react-router-dom'
import { Elinks, mapperURL } from '../../mappers/URL.mapper'

import * as S from './styles'

interface NavigationBarProps {
  links: string[]
}

const NavigationBar: React.FC<NavigationBarProps> = ({ links }) => {
  const location = useLocation()

  return (
    <S.Container>
      {links.map((link, index) => {
        return (
          <S.Options
            href={mapperURL[link as Elinks]}
            isActive={location.pathname === mapperURL[link as Elinks] ? 1 : 0}
            key={`link-${index}`}
          >
            {link}
          </S.Options>
        )
      })}
    </S.Container>
  )
}

export default NavigationBar
