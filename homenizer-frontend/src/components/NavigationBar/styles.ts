import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  padding: 1rem;

  :first-child {
    padding-left: 0;
  }

  :last-child {
    padding-right: 0;
  }
`
interface OptionsProps {
  isActive: number
}

export const Options = styled.a<OptionsProps>`
  padding: 0 1.5rem;
  color: ${({ isActive, theme }) =>
    isActive === 1 ? `${theme.colors.blackDisabled}` : `${theme.colors.black}`};
  text-decoration: none;
  font-size: 1.375rem;
  font-weight: 600;

  :hover {
    transition: all 0.2s ease-in-out;
    cursor: ${({ isActive }) => (isActive === 1 ? 'not-allowed' : 'pointer')};
    border-bottom: ${({ isActive }) => isActive !== 1 && '3px solid orange'};
  }
`
