import styled from 'styled-components'

export const Container = styled.div`
  max-width: 14.31rem;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.backgroundPrimary};
  border-right: 1px solid ${({ theme }) => theme.colors.blackDivider};
`

export const LinkContainer = styled.div`
  width: 100%;
  text-align: center;
  padding-top: 9.75rem;
`
interface LinkProps {
  disabled: boolean
}

export const Link = styled.div<LinkProps>`
  font-size: 1.5rem;
  font-weight: 600;
  padding: 1rem 0;
  a {
    color: ${({ theme, disabled }) =>
      !disabled ? `${theme.colors.black}` : `${theme.colors.blackDisabled}`};
    text-decoration: none;
    cursor: ${({ disabled }) => (!disabled ? 'pointer' : 'not-allowed')};
    :hover {
      text-decoration: ${({ disabled }) => !disabled && 'underline'};
    }
  }
`
