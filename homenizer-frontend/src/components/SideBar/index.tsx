import React from 'react'
import { useLocation } from 'react-router-dom'
import {
  linkSideBarOptionsMapper,
  sideBarOptionsLinkMapper
} from '../../mappers/sideBarOptions.mapper'
import * as S from './styles'

interface SideBarProps {
  options: string[]
}

const SideBar: React.FC<SideBarProps> = ({ options }) => {
  const location = useLocation()

  const linkIsActive = () => linkSideBarOptionsMapper(location.pathname)

  return (
    <S.Container>
      <S.LinkContainer>
        {options.map((option) => {
          return (
            <S.Link key={`link-${option}`} disabled={linkIsActive() === option}>
              <a target="_parent" href={`${sideBarOptionsLinkMapper(option)}`}>
                {option}
              </a>
            </S.Link>
          )
        })}
      </S.LinkContainer>
    </S.Container>
  )
}

export default SideBar
