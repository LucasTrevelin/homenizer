import styled from 'styled-components';

export const Container = styled.div`
  border-top: 1px solid ${({theme}) => theme.colors.black };
  width: 100%
`;
