import React, { useContext } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { UserContext, useUserContext } from '../../context/User.context'
import { headerNavigation } from '../../mappers/headerNavigation.mapper'
import { EURLs } from '../../mappers/URL.mapper'
import { IUserResponse } from '../../models/User.model'
import AvatarProfile from '../AvatarProfile'
import NavigationBar from '../NavigationBar'
import * as S from './styles'

interface HeaderProps {
  hasNavigation?: boolean
}

const Header: React.FC<HeaderProps> = ({ hasNavigation }) => {
  const location = useLocation()
  const navigate = useNavigate()

  const { loggedUser } = useUserContext()

  const savedUsername =
    (JSON.parse(localStorage.getItem('homenizer-app')!) as IUserResponse)
      .username || ''

  return (
    <S.Container>
      <S.HomeButton onClick={() => navigate('/home')}>
        <img
          width="70.4px"
          height="70.4px"
          alt="small-logo"
          src="/assets/images/logos/small_logo.png"
        />
      </S.HomeButton>
      <S.NavigationContainer>
        {hasNavigation ? (
          <NavigationBar
            links={headerNavigation[location.pathname as EURLs].options}
          />
        ) : undefined}
        <AvatarProfile
          type="medium"
          userName={loggedUser?.username || savedUsername}
          isClickable
        />
      </S.NavigationContainer>
    </S.Container>
  )
}

export default Header
