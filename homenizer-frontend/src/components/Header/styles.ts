import styled from 'styled-components'

export const Container = styled.header`
  height: 6rem;
  background-color: ${({ theme }) => theme.colors.header};
  padding: 0.5rem 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
export const NavigationContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  row-gap: 2rem;
`
export const HomeButton = styled.button`
  background-color: transparent;
  border: none;
  z-index: 5;
  cursor: pointer;
`
