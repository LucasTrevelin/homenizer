module.exports = async () => ({
  preset: 'ts-jest/presets/js-with-ts',

  testEnvironment: 'jsdom',

  globals: {
    'ts-jest': {
        tsconfig: '<rootDir>/test/tsconfig.json',
    },
},

  verbose: true,

  collectCoverage: true,

  modulePaths: ['src'],

  jest: {
    URL: "http://test.com/"
  },

  testPathIgnorePatterns: ['<rootDir>[/\\\\](node_modules|.next)[/\\\\]'],
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(ts|tsx)$'],
  transform: {
    '^.+\\.(ts|tsx|js|jsx)$': 'babel-jest',
  },
  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname',
  ],
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': 'identity-obj-proxy',
    '\\.(gif|ttf|eot|svg|png)$': '<rootDir>/test/__mocks__/fileMock.js',
    'src/(.*)': '<rootDir>/src/$1',
    '@/pages/(.*)': '<rootDir>/src/pages/$1',
    '@/components/(.*)': '<rootDir>/src/components/$1',
  },

  // The root of your source code, typically /src
  // `<rootDir>` is a token Jest substitutes
  roots: ['<rootDir>/src/'],

  // Jest transformations -- this adds support for TypeScript
  // using ts-jest
  transform: {
    "node_modules/variables/.+\\.(j|t)sx?$": "ts-jest",
    '^.+\\.(tsx|ts|jsx|js)?$': 'ts-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|svg|ttf|woff|woff2)$':
      'jest-transform-stub',
  },

  //transformIgnorePatterns: [
  //  "node_modules/(?!variables/.*)",'node_modules/(?!(redux-persist)/)'],

  // Runs special logic, such as cleaning up components
  // when using React Testing Library and adds special
  // extended assertions to Jest
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts', "react-testing-library/cleanup-after-each"],

  // Test spec file resolution pattern
  // Matches parent folder `__test__` and filename
  // should contain `test`.
  testRegex: '(/__test__/.*|(\\.|/)(test))\\.tsx?$',

  //testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/cypress/'],

  modulePathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/cypress/'],

  // ignore icons and storybook from coverage
  collectCoverageFrom: ['**/*.tsx', '!**/node_modules/**', '!**/cypress/**'],

  // Module file extensions for importing
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node', 'mts'],
});